/*
** rensyu_hamada.c
** 入力した2つの整数区間に含まれる素数の総和を出力する
*/
#include <stdio.h>

// 閉区間[start, end]
typedef struct
{
	int start;
	int end;
} Interval;

// 2つの整数を入力し、区間として返す
Interval set();
// 閉区間[a, b]に含まれる素数の総和
int sum_primes(int a, int b);
// 結果の出力
void output(int result);
// 素数判定
// int IsPrimeNum(int x);

int main()
{
	Interval in = set();
	int result = sum_primes(in.start, in.end);
	output(result);
	return 0;
}

Interval set()
{
	Interval setter;
	int s, e, temp;
	printf("input int numbers\n");
	scanf("%d", &s);
	scanf("%d", &e);
	// 大小整理
	if (s > e)
	{
		temp = s;
		s = e;
		e = temp;
	}
	setter.start = s;
	setter.end = e;
	return setter;
}

int sum_primes(int a, int b)
{
	int sum = 0;
	int i, j;

	// 1を加算しないように2以上から始める
	for (i = (a < 2 ? 2 : a); i <= b; ++i)
	{
		for (j = 2; j*j <= i; ++j)
		{
			if (i % j == 0)
			{
				break;
			}
		}

		// breakされずにfor文が終了していれば、素数なので和に加算
		if (j*j > i)
		{
			sum += i;
			// printf("%d ", i); // デバッグ：加算した値の表示
		}
	}
	return sum;
}

void output(int result)
{
	printf("the result is %d.\n", result);
}

/*
int IsPrimeNum(int x)
{
	int i;

	if (i == 1)
	{
		return 0;
	}

	for (i = 2; i*i < x; ++i)
	{
		if (x % i == 0)
		{
			return 0;
		}
	}
	return 1;
}
*/
