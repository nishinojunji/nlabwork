/*
** 濱田信佑
** rensyu1_hamada.c
**
** 入力した2つの正整数区間に含まれる素数の総和を出力する
**
** CED環境で走らせて
**  1000000000000000 で 13.3秒
** 10000000000000000 で 60.9秒
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// 閉区間[start, end]
typedef struct
{
	long int start;
	long int end;
} Interval;

// 2つの整数を入力し、区間として返す
Interval set();
// 閉区間[a, b]に含まれる素数の総和
long int sum_primes(long int a, long int b);
// 結果の出力
void output(long int result);

int main()
{
	Interval in = set();
	long int result = sum_primes(in.start, in.end);
	output(result);
	return 0;
}

Interval set()
{
	Interval setter;
	long int s, e, temp;
	printf("input 2 int numbers.\n");
	scanf("%ld", &s);
	scanf("%ld", &e);
	// 大小整理
	if (s > e)
	{
		temp = s;
		s = e;
		e = temp;
	}
	setter.start = s;
	setter.end = e;
	return setter;
}

long int sum_primes(long int a, long int b)
{
	long int sum = 0;
	long int i, j;

	const long int INT_SIZE = sizeof(long int);
	long int capa;				// 確保済みの素数リストのサイズ
	long int list_num;			// リストに保存された素数の数
	long int* prime_list;		// 素数のリスト
	long int* temp;				// リスト拡張時の一時リスト
	long int sqrt_i, sqrt_b;	//sqrt(x)呼び出し省略用

	// ---- 初期化 ----
	capa = 1000000;
	prime_list = (long int*)malloc(capa*INT_SIZE);
	prime_list[0] = 2;
	// prime_list[1] = 3;
	list_num = 1;
	// ---------------

	// ---- 素数リスト作成 ----
	// 区間の終点の平方根より小さい素数のリストを作る
	sqrt_b = (long int)sqrt(b);
	printf("\nmaking a list of prime numbers up to %ld\n\nprogress ...\n", sqrt_b);
	for (i = prime_list[list_num-1] + 1; i <= sqrt_b; ++i)
	{
		// 素数リストを昇順で探索
		sqrt_i = (long int)sqrt(i);
		for (j = 0; prime_list[j] <= sqrt_i; ++j)
		{
			if (i % prime_list[j] == 0)
			{
				goto skip_add_list;
			}
		}

		if (list_num >= capa)
		{
			temp = (long int*)realloc(prime_list, capa*2*INT_SIZE);
			if (temp != NULL)
			{
				prime_list = temp;
				capa *= 2;
			} 
			else
			{
				printf("error\n");
				exit(0);
			}
		}
		prime_list[list_num] = i;
		++list_num;

	skip_add_list:
		// リスト作成進捗表示
		if (sqrt_b / 10 > 0)
		{
			if (i % (sqrt_b / 10) == 0)
			{
				printf("%ld0%%\n",i / (sqrt_b / 10));
			}
		}
		// continue;
	}
	//----------------------

	//デバッグ用出力
	printf("completed.\n");
	// printf("\nprime_list :\n");
	// for (i = 0; i < list_num; ++i) printf("%ld ", prime_list[i]);
	// printf("\n\n");
	// printf("making list is successful (list_num %ld).\n", list_num);

	//---- 区間内にある素数の加算 ----
	a = (a < 2 ? 2 : a);
	for (i = a; i <= b; ++i)
	{
		sqrt_i = (long int)sqrt(i);
		for (j = 0; j < list_num && prime_list[j] <= sqrt_i; ++j)
		{
			if (i % prime_list[j] == 0)
			{
				goto skip_add_sum;
			}
		}
		// printf("%ld ", i);
		sum += i;

	skip_add_sum:
		continue;
	}
	//-------------------------

	return sum;
}

void output(long int result)
{
	printf("\nthe result is %ld.\n\n", result);
}
