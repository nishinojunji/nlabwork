//M1佐藤、B4山本組
//課題内容：ルービックキューブを解く
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//--- structure ---//
typedef struct{
  //int b_color[24];//各キューブブロックの色。1:白、2:赤、3:青、4:緑、5:黃、6:橙
  int b1_color[4];//白
  int b2_color[4];//赤
  int b3_color[4];//青
  int b4_color[4];//緑
  int b5_color[4];//黃
  int b6_color[4];//橙
} Cube;
typedef struct{
  int block;//動かすブロック
  int move;//回す回転数と方向
} Enter;//人間によるキューブの操作入力
//--- property --//
int Display_cube(Cube c);//キューブの表示関数
Cube Move_Cube(Cube c,Enter i);//キューブを動かす関数。
Cube Init_Cube(void);//キューブの初期配置の関数
int Trans_Color(int num);//numの値によって色のアルファベットを返す。
int Finish(void);//ブロック操作の入力終了
Enter Enter_Move(void);//ブロックの動かす情報入力
int isNumber(char str[]);
int OK_NO(void);
int S_Roop(int t,int m,int n);

//--- main ---//
int main(void){

  Cube c;
  Enter i;
  int f;


  c = Init_Cube();
  Display_cube(c);
  while(1){
    i = Enter_Move();
    c = Move_Cube(c,i);
    Display_cube(c);
    f = OK_NO();
    if(f==1) break;
  }

  return 0;
}

//--- function ---//
int Display_cube(Cube c){

   int i;

   printf("  \n \t\t%d\t%d\n ",c.b1_color[0],c.b1_color[1]);
  printf("  \t\t%d\t%d\n",c.b1_color[2],c.b1_color[3]);
  // printf("\x1b[46m");　これは色をつけるコマンド
  printf("  %d\t%d\t%d\t%d\t%d\t%d\t",c.b2_color[0],c.b2_color[1],c.b3_color[0],c.b3_color[1],c.b4_color[0],c.b4_color[1]);
  printf("\n");
  printf("\ %d\t%d\t%d\t%d\t%d\t%d\t",c.b2_color[2],c.b2_color[3],c.b3_color[2],c.b3_color[3],c.b4_color[2],c.b4_color[3]);
  printf("\n");
  printf(" \t\t%d\t%d\n",c.b5_color[0],c.b5_color[1]);
  printf("  \t\t%d\t%d\n",c.b5_color[2],c.b5_color[3]);
  printf(" \t\t%d\t%d\n",c.b6_color[0],c.b6_color[1]);
  printf(" \t\t%d\t%d\n",c.b6_color[2],c.b6_color[3]); 

 printf("\x1b[39m"); 
  return 0;
}

Cube Move_Cube(Cube c,Enter i){

    int num;
  int a1,a2,a3,a4,a5,a6,a7,a8;

  
  switch(i.block){

  case 1:
    a1 = c.b1_color[1]; 
    a2 = c.b1_color[3];
    a3 = c.b3_color[1];
    a4 = c.b3_color[3];
    a5 = c.b5_color[1];
    a6 = c.b5_color[3];
    a7 = c.b6_color[1];
    a8 = c.b6_color[3];

    if(i.move==1||i.move==-3){
      c.b1_color[1] = a3;
      c.b1_color[3] = a4;
      c.b3_color[1] = a5;
      c.b3_color[3] = a6;
      c.b5_color[1] = a7;
      c.b5_color[3] = a8;
      c.b6_color[1] = a1;
      c.b6_color[3] = a2;
    }
    if(i.move==2||i.move==-2){
      c.b1_color[1] = a5;
      c.b1_color[3] = a6;
      c.b3_color[1] = a7;
      c.b3_color[3] = a8;
      c.b5_color[1] = a1;
      c.b5_color[3] = a2;
      c.b6_color[1] = a3;
      c.b6_color[3] = a4;
    }
    if(i.move==3||i.move==-1){
      c.b1_color[1] = a7;
      c.b1_color[3] = a8;
      c.b3_color[1] = a1;
      c.b3_color[3] = a2;
      c.b5_color[1] = a3;
      c.b5_color[3] = a4;
      c.b6_color[1] = a5;
      c.b6_color[3] = a6;
    }
  case 2:break;
  case 3:break;
  default:break;
  }
  
  
  return c;
}

Cube Init_Cube(void){
   Cube c;
     int i;
  int num = 0;

  for(i=0;i<4;i++){
    num = 1;
    c.b1_color[i] = num;  
  }

  for(i=0;i<4;i++){
    num = 2;
    c.b2_color[i] = num;  
  }

  for(i=0;i<4;i++){
    num = 3;
    c.b3_color[i] = num;  
  }

  for(i=0;i<4;i++){
    num = 4;
    c.b4_color[i] = num;  
  }

  for(i=0;i<4;i++){//i<5だった
    num = 5;
    c.b5_color[i] = num;  
  }

  for(i=0;i<4;i++){//i<6だった
    num = 6;
    c.b6_color[i] = num;  
  }
    //for(i=0;i<24;i++) printf("%d\n",c.b_color[i]);//必要なし
  return c;
}
int Trans_Color(int num){

  return 0;
}

int Finish(void){//ブロック操作の入力終了

   int f;
   
 char i[3];

  while(1){
  printf("ブロック操作を終了しますか?(yes=1/no=0)\n");
  scanf("%s",i);
  if(isNumber(i)==1){
    if(S_Roop(atoi(i),0,1) == 0){
      printf("入力を正しく行ってください\n");
    }else{
      break;
    }
  }else{
    printf("正しい入力をしてください\n");
  }
  }
  f = atoi(i); 
  return f;
}

Enter Enter_Move(void){

  Enter e;
  char b[3];
  int m;

  while(1){
  printf("動かすブロックの列の入力\n");
  printf("1:基準ブロックの右隣のブロック列\n2:基準ブロックの上野ブロック列\n3:基準ブロックの奥のブロック列\n");
  scanf("%s",b);
  if(isNumber(b)==1){
    if(S_Roop(atoi(b),1,3) == 0){
      printf("入力を正しく行ってください\n");
    }else{
      break;
    }
  }else{
    printf("正しい入力をしてください\n");
  }
  }

  while(1){
  printf("動かす方向とブロック数\n");
  printf("基準ブロックから見て動かす方向が上か右ならば正の値\n基準ブロックから見て動かす方向が下か左ならば負の値\n");
  scanf("%d",&m);
    if(S_Roop(m,-4,4) == 0){
      printf("入力を正しく行ってください\n");
    }else{
      break;
    }
  }
  e.block = atoi(b);
  e.move = m;

  return e;
}

int isNumber(char str[]){
  char ch;
  int rp = 0;
  do{
    ch = str[rp];

    if(!(isdigit(ch))){
      return 0;
    }else if(rp == 0 && ch=='0'){
      if(str[rp+1] != '\0'){
	return 0;
      }
    }
    rp += 1;
  }while(str[rp] != '\0');

  return 1;
}

int OK_NO(void){
  char i[2];
  while(1){
    printf("入力を終了しますか?(yes=1/no=0)\n");
    scanf("%s",i);
    if(isNumber(i)==1){
      if(atoi(i)==1){
	return 1;
      }
      else if(atoi(i)==0){
	printf("入力を最初からやり直します\n");
	return 0;
      }
      else{
	printf("正しい入力をしてください\n");
      }
    }else{
      printf("正しい入力をしてください\n");
    }
  }
}
int S_Roop(int t,int m,int n){

  int i;
  
  for(i=m;i<=n;i++){
    if(t == i) return 1;
  }
  return 0;
}



