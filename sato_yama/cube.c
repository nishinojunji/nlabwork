//M1佐藤、B4山本組
//課題内容：ルービックキューブを解く
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 100

//--- structure ---//
typedef struct{
  //int b_color[24];//各キューブブロックの色。1:白、2:赤、3:青、4:緑、5:黃、6:橙
  int b1_color[4];//白
  int b2_color[4];//赤
  int b3_color[4];//青
  int b4_color[4];//緑
  int b5_color[4];//黃
  int b6_color[4];//橙
} Cube;

//--- property --//
int Start_Step(void);//実験開始を知らせる画面の出力関数
int Display_Cube(Cube c);//キューブの表示関数
Cube Move_Cube(Cube c,int i);//キューブを動かす関数。
Cube Init_Cube(void);//キューブの初期配置の関数
void print_move(int n);//今までの操作の内容を出力する関数
void dfs(int n,Cube c,Cube s);//深さ優先探索、nはコマンドを記憶するための変数
int isNumber(char str[]);
int OK_NO(void);
int S_Roop(int t,int m,int n);
Cube Zero_Cube(void);
int isGoal(Cube c);
int Enter_Cube(int x);
Cube Input_Cube(int i,int x1,int x2,int x3,int x4,Cube c);
//--- global variable ---//
int path[N];


//--- main ---//
int main(void){

  Cube c;//色の位置をいじっていく変数
  Cube s;//最初のルービックキューブの色の位置を記憶する変数

  Start_Step();//実験開始を知らせる画面
  c = Init_Cube();//変更の必要あり
  s = c;
  Display_Cube(c);
  dfs(0,c,s);

  return 0;
}

//--- function ---//
void print_move(int n){

  int i;

  for(i=0;i<n;i++){
    printf("%d\t",path[i]);
  }
  printf("\n");
}

void dfs(int n,Cube c,Cube s){//深さ優先探索、nはコマンドを記憶するための変数

  int i;

  if(isGoal(c)==1){//もしキューブが全面そろっていたらそれまでの工程を出力
    print_move(n);
  }else{
    for(i=1;i<4;i++){
      c = Move_Cube(c,i);//キューブを動かす
      if(isStart(c,s)==1) break;//スタートした時と同じ形になっていたら、forを抜ける
      path[n] = i; 
      dfs(n+1,c,s);
    }//forの終わり
  }//elseの終わり
}//dfsの終わり

int isStart(Cube c,Cube s){

  int i;
  int j;

  for(i=0;i<4;i++){
    if((c.b1_color[i]==s.b1_color[i])
     &&(c.b2_color[i]==s.b2_color[i])
     &&(c.b3_color[i]==s.b3_color[i])
     &&(c.b4_color[i]==s.b4_color[i])
     &&(c.b5_color[i]==s.b5_color[i])
     &&(c.b6_color[i]==s.b6_color[i])){
      j = 1;
    }else{
      j=0;
      break;
    }
  }

  return j;

}


int isGoal(Cube c){

  int x1,x2,x3,x4,x5,x6;
  int t;

  x1 = fourCheck(c.b1_color[0],c.b1_color[1],c.b1_color[2],c.b1_color[3]);
  x2 = fourCheck(c.b2_color[0],c.b2_color[1],c.b2_color[2],c.b2_color[3]);
  x3 = fourCheck(c.b3_color[0],c.b3_color[1],c.b3_color[2],c.b3_color[3]);
  x4 = fourCheck(c.b4_color[0],c.b4_color[1],c.b4_color[2],c.b4_color[3]);
  x5 = fourCheck(c.b5_color[0],c.b5_color[1],c.b5_color[2],c.b5_color[3]);
  x6 = fourCheck(c.b6_color[0],c.b6_color[1],c.b6_color[2],c.b6_color[3]);

  t = x1 + x2 + x3 + x4 + x5 + x6;

  if(t==6) return 1;
  else return 0;


}

int fourCheck(int a,int b,int c,int d){

  if((a==b)&&(c==d)&&(a==c)) return 1;
  else return 0;
}

int Display_Cube(Cube c){

  int i;
  printf("\t\t%d\t%d\n",c.b1_color[0],c.b1_color[1]);
  printf("\t\t%d\t%d\n",c.b1_color[2],c.b1_color[3]);
  printf("%d\t%d\t%d\t%d\t%d\t%d\t",c.b2_color[0],c.b2_color[1],c.b3_color[0],c.b3_color[1],c.b4_color[0],c.b4_color[1]);
  printf("\n");
  printf("%d\t%d\t%d\t%d\t%d\t%d\t",c.b2_color[2],c.b2_color[3],c.b3_color[2],c.b3_color[3],c.b4_color[2],c.b4_color[3]);
  printf("\n");
  printf("\t\t%d\t%d\n",c.b5_color[0],c.b5_color[1]);
  printf("\t\t%d\t%d\n",c.b5_color[2],c.b5_color[3]);
  printf("\t\t%d\t%d\n",c.b6_color[0],c.b6_color[1]);
  printf("\t\t%d\t%d\n",c.b6_color[2],c.b6_color[3]);
  return 0;
}

Cube Move_Cube(Cube c,int i){

  int num;
  int a1,a2,a3,a4,a5,a6,a7,a8;

  
  switch(i){

  case 1:

    a1 = c.b1_color[1];
    a2 = c.b1_color[3];
    a3 = c.b3_color[1];
    a4 = c.b3_color[3];
    a5 = c.b5_color[1];
    a6 = c.b5_color[3];
    a7 = c.b6_color[1];
    a8 = c.b6_color[3];
    
    c.b1_color[1] = a3;
    c.b1_color[3] = a4;
    c.b3_color[1] = a5;
    c.b3_color[3] = a6;
    c.b5_color[1] = a7;
    c.b5_color[3] = a8;
    c.b6_color[1] = a1;
    c.b6_color[3] = a2;
    break;
  case 2:

    a1 = c.b2_color[0];
    a2 = c.b2_color[1];
    a3 = c.b3_color[0];
    a4 = c.b3_color[1];
    a5 = c.b4_color[0];
    a6 = c.b4_color[1];
    a7 = c.b6_color[2];
    a8 = c.b6_color[3];

    c.b2_color[0] = a7;
    c.b2_color[1] = a8;
    c.b3_color[0] = a1;
    c.b3_color[1] = a2;
    c.b4_color[0] = a3;
    c.b4_color[1] = a4;
    c.b6_color[2] = a5;
    c.b6_color[3] = a6;
    break;
  case 3:
    a1 = c.b1_color[0];
    a2 = c.b1_color[1];
    a3 = c.b2_color[2];
    a4 = c.b2_color[0];
    a5 = c.b4_color[1];
    a6 = c.b4_color[3];
    a7 = c.b5_color[3];
    a8 = c.b5_color[2];

    c.b1_color[0] = a3;
    c.b1_color[1] = a4;
    c.b2_color[2] = a7;
    c.b2_color[0] = a8;
    c.b4_color[1] = a1;
    c.b4_color[3] = a2;
    c.b5_color[3] = a5;
    c.b5_color[2] = a6;
    break;
  default:break;
  }
  return c;
}

Cube Init_Cube(void){
  Cube c;
  int i;
  int num = 0;
  char a[3];
  int x1,x2,x3,x4;

  c = Zero_Cube();
  Display_Cube(c);
  for(i=1;i<7;i++){
    printf("ブロック面b%dの配色を入力してください\n",i);
    while(1){
    x1 = Enter_Cube(1);
    x2 = Enter_Cube(2);
    x3 = Enter_Cube(3);
    x4 = Enter_Cube(4);
    c = Input_Cube(i,x1,x2,x3,x4,c);
    Display_Cube(c);
    if(OK_NO()==1) break;
    }
  }//forの終わり
    
  return c;
}

int Enter_Cube(int x){
 
  char a[3];

  printf("1<-->白\n2<-->赤\n3<-->青\n4<-->緑\n5<-->黃\n6<-->橙\n");
  switch(x){
  case 1: 
    printf("左上のブロックの色::");
      break;
  case 2:
    printf("右上のブロックの色::");
    break;
  case 3:
    printf("左下のブロックの色::");
    break;
  case 4:
    printf("右下のブロックの色::");
    break;
  default:break;
  }
  while(1){
      scanf("%s",a);
      if(isNumber(a)==1){
	if(S_Roop(atoi(a),1,6)==0){
	  printf("入力を正しく行ってください\n");
	}else{
	  break;
	}//if(S_Roop)の終わり
      }else{
	printf("正しい入力をしてください");
      }//isNumberの終わり
  }//while(1)の終わり
  
  return atoi(a);
  
}

Cube Input_Cube(int i,int x1,int x2,int x3,int x4,Cube c){

  switch(i){
  case 1:
    c.b1_color[0]=x1;
    c.b1_color[1]=x2;
    c.b1_color[2]=x3;
    c.b1_color[3]=x4;
    break;
  case 2:
    c.b2_color[0]=x1;
    c.b2_color[1]=x2;
    c.b2_color[2]=x3;
    c.b2_color[3]=x4;
    break;
  case 3:
    c.b3_color[0]=x1;
    c.b3_color[1]=x2;
    c.b3_color[2]=x3;
    c.b3_color[3]=x4;
    break;
  case 4:
    c.b4_color[0]=x1;
    c.b4_color[1]=x2;
    c.b4_color[2]=x3;
    c.b4_color[3]=x4;
    break;
  case 5:
    c.b5_color[0]=x1;
    c.b5_color[1]=x2;
    c.b5_color[2]=x3;
    c.b5_color[3]=x4;
    break;
  case 6:
    c.b6_color[0]=x1;
    c.b6_color[1]=x2;
    c.b6_color[2]=x3;
    c.b6_color[3]=x4;
    break;
  default:break;
  }

  return c;
}

Cube Zero_Cube(void){

  Cube c;
  int i;
  int num = 0;

  for(i=0;i<4;i++){
    c.b1_color[i]=num;
    c.b2_color[i]=num;
    c.b3_color[i]=num;
    c.b4_color[i]=num;
    c.b5_color[i]=num;
    c.b6_color[i]=num;
  }

  return c;
}

int Start_Step(void){

  printf("ルービックキューブを動かすシミュレーション実験を開始します\n");

  printf("ルービックキューブの色と数字の対応\n");
  printf("1<-->白\n2<-->赤\n3<-->青\n4<-->緑\n5<-->黃\n6<-->橙\n");
  printf("実験中に扱うブロック操作のコマンドの意味\n");
  printf("1:基準ブロックの右隣のブロック列を上に動かす\n2:基準ブロックの上のブロック列を右に動かす\n3:基準ブロックの奥のブロック列を右に動かす\n");

  printf("それでは実験を開始します\n");

  return 0;
}


int isNumber(char str[]){
  char ch;
  int rp = 0;
  do{
    ch = str[rp];

    if(!(isdigit(ch))){
      return 0;
    }else if(rp == 0 && ch=='0'){
      if(str[rp+1] != '\0'){
	return 0;
      }
    }
    rp += 1;
  }while(str[rp] != '\0');

  return 1;
}

int OK_NO(void){
  char i[2];
  while(1){
    printf("入力を終了しますか?(yes=1/no=0)\n");
    scanf("%s",i);
    if(isNumber(i)==1){
      if(atoi(i)==1){
	return 1;
      }
      else if(atoi(i)==0){
	printf("入力を最初からやり直します\n");
	return 0;
      }
      else{
	printf("正しい入力をしてください\n");
      }
    }else{
      printf("正しい入力をしてください\n");
    }
  }
}
int S_Roop(int t,int m,int n){

  int i;
  
  for(i=m;i<=n;i++){
    if(t == i) return 1;
  }
  return 0;
}





