#include<stdio.h>

/******prototypes******/
typedef struct {char c[24];} cube;
typedef struct {char men; int count;} move;

cube cube_init(cube c);
void cube_display(cube c);
move user(move m);
cube cube_move(cube c, move m);

/******main******/
int main(){
  cube c;
  move m;
  c = cube_init(c);
  cube_display(c);
  while(1){
    m = user(m);
    c = cube_move(c,m);
    cube_display(c);  
  }
  return 0;
}

cube cube_init(cube c)
{
  c.c[0]='y'; c.c[1]='y';
  c.c[2]='y'; c.c[3]='y';
  c.c[4]='b'; c.c[5]='b'; c.c[6]='w'; c.c[7]='w'; c.c[8]='r'; c.c[9]='r';
  c.c[10]='b'; c.c[11]='b'; c.c[12]='w'; c.c[13]='w'; c.c[14]='r'; c.c[15]='r';
  c.c[16]='g'; c.c[17]='g';
  c.c[18]='g'; c.c[19]='g';
  c.c[20]='o'; c.c[21]='o';
  c.c[22]='o'; c.c[23]='o';
  return c;
}

void cube_display(cube c)
{
  printf("  %c%c  \n", c.c[0], c.c[1]);
  printf("  %c%c  \n", c.c[2], c.c[3]);
  printf("%c%c%c%c%c%c\n", c.c[4], c.c[5], c.c[6], c.c[7], c.c[8], c.c[9]);
  printf("%c%c%c%c%c%c\n", c.c[10], c.c[11], c.c[12], c.c[13], c.c[14], c.c[15]);
  printf("  %c%c  \n", c.c[16], c.c[17]);
  printf("  %c%c  \n", c.c[18], c.c[19]);
  printf("  %c%c  \n", c.c[20], c.c[21]);
  printf("  %c%c  \n", c.c[22], c.c[23]);
}

move user(move m){
  char dummy;
  printf("回転面を入力[a,b,c]\n");
  scanf("%c", &m.men);
  printf("回転回数を入力[1~3]\n");
  scanf("%d", &m.count);
  scanf("%c", &dummy);

  return m;
}

cube cube_move(cube c, move m){
  int i;
  char temp[2];

  for(i=0; i<m.count; i++){//回転回数
    if(m.men == 'a'){
      temp[0] = c.c[4];
      temp[1] = c.c[5];
      c.c[4] = c.c[6];
      c.c[5] = c.c[7];
      c.c[6] = c.c[8];
      c.c[7] = c.c[9];
      c.c[8] = c.c[23];
      c.c[9] = c.c[22];
      c.c[23] = temp[0];
      c.c[22] = temp[1];
      temp[0] = c.c[0];
      c.c[0] = c.c[2];
      c.c[2] = c.c[3];
      c.c[3] = c.c[1];
      c.c[1] = temp[0];
    }else if(m.men == 'b'){
      temp[0] = c.c[1];
      temp[1] = c.c[3];
      c.c[1] = c.c[7];
      c.c[3] = c.c[13];
      c.c[7] = c.c[17];
      c.c[13] = c.c[19];
      c.c[17] = c.c[21];
      c.c[19] = c.c[23];
      c.c[21] = temp[0];
      c.c[23] = temp[1];
      temp[0] = c.c[8];
      c.c[8] = c.c[14];
      c.c[14] = c.c[15];
      c.c[15] = c.c[9];
      c.c[9] = temp[0];
    }else if(m.men == 'c'){
      temp[0] = c.c[0];
      temp[1] = c.c[1];
      c.c[0] = c.c[9];
      c.c[1] = c.c[15];
      c.c[9] = c.c[19];
      c.c[15] = c.c[18];
      c.c[19] = c.c[10];
      c.c[18] = c.c[4];
      c.c[10] = temp[0];
      c.c[4] = temp[1];
      temp[0] = c.c[22];
      c.c[22] = c.c[23];
      c.c[23] = c.c[21];
      c.c[21] = c.c[20];
      c.c[20] = temp[0];
    }
  }

  return c;
}
