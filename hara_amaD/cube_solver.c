#include<stdio.h>
#include<stdlib.h>

/******prototypes******/
typedef struct {char c[26];} cube;
typedef struct {char men; int count;} move;
struct node{cube c; move m; struct node* next; struct node* pre;};

cube cube_init(cube c);
void cube_display(cube c);
move user(move m);
cube cube_move(cube c, move m);
struct node* init_queue();
struct node* enqueue(struct node* rear, struct node* pre, cube c, move m);
struct node* dequeue(struct node* front);
int check_correct(cube c);
int check_color(cube c, int n1, int n2, int n3, int n4);
void print_solve(struct node* n);
struct node* cube_solver(cube c);
int check_same(cube c, struct node* n);

/******main******/
int main(){
  cube c;
  move m;
  struct node* solve;
  c = cube_init(c);
  //cube_display(c);

  solve = cube_solver(c);
  print_solve(solve);
  return 0;
}

/******function******/
cube cube_init(cube c)
{
  printf("Input initial cube\n");
  scanf("%26s", c.c);
  return c;
}

void cube_display(cube c)
{
  printf("  %c%c  \n", c.c[0], c.c[1]);
  printf("  %c%c  \n", c.c[2], c.c[3]);
  printf("%c%c%c%c%c%c\n", c.c[4], c.c[5], c.c[6], c.c[7], c.c[8], c.c[9]);
  printf("%c%c%c%c%c%c\n", c.c[10], c.c[11], c.c[12], c.c[13], c.c[14], c.c[15]);
  printf("  %c%c  \n", c.c[16], c.c[17]);
  printf("  %c%c  \n", c.c[18], c.c[19]);
  printf("  %c%c  \n", c.c[20], c.c[21]);
  printf("  %c%c  \n", c.c[22], c.c[23]);
}

move user(move m){
  char dummy;
  printf("回転面を入力[a,b,c]\n");
  scanf("%c", &m.men);
  printf("回転回数を入力[1~3]\n");
  scanf("%d", &m.count);
  scanf("%c", &dummy);

  return m;
}

cube cube_move(cube c, move m){
  int i;
  char temp[2];

  for(i=0; i<m.count; i++){//回転回数
    if(m.men == 'a'){
      temp[0] = c.c[4];
      temp[1] = c.c[5];
      c.c[4] = c.c[6];
      c.c[5] = c.c[7];
      c.c[6] = c.c[8];
      c.c[7] = c.c[9];
      c.c[8] = c.c[23];
      c.c[9] = c.c[22];
      c.c[23] = temp[0];
      c.c[22] = temp[1];
      temp[0] = c.c[0];
      c.c[0] = c.c[2];
      c.c[2] = c.c[3];
      c.c[3] = c.c[1];
      c.c[1] = temp[0];
    }else if(m.men == 'b'){
      temp[0] = c.c[1];
      temp[1] = c.c[3];
      c.c[1] = c.c[7];
      c.c[3] = c.c[13];
      c.c[7] = c.c[17];
      c.c[13] = c.c[19];
      c.c[17] = c.c[21];
      c.c[19] = c.c[23];
      c.c[21] = temp[0];
      c.c[23] = temp[1];
      temp[0] = c.c[8];
      c.c[8] = c.c[14];
      c.c[14] = c.c[15];
      c.c[15] = c.c[9];
      c.c[9] = temp[0];
    }else if(m.men == 'c'){
      temp[0] = c.c[0];
      temp[1] = c.c[1];
      c.c[0] = c.c[9];
      c.c[1] = c.c[15];
      c.c[9] = c.c[19];
      c.c[15] = c.c[18];
      c.c[19] = c.c[10];
      c.c[18] = c.c[4];
      c.c[10] = temp[0];
      c.c[4] = temp[1];
      temp[0] = c.c[22];
      c.c[22] = c.c[23];
      c.c[23] = c.c[21];
      c.c[21] = c.c[20];
      c.c[20] = temp[0];
    }
  }
  return c;
}

struct node* init_queue()
{
  struct node* n=(struct node*)malloc(sizeof(struct node));
  n->next = n->pre = NULL;
  return n;
}

struct node* enqueue(struct node* rear, struct node* pre, cube c, move m)
{
  struct node* n = init_queue();
  n->c = c;  n->m = m;  n->pre = pre;
  n->next = rear->next;  rear->next = n;
  return n;
}

struct node* dequeue(struct node* front)
{
  struct node* n = front->next;
  front->next = front->next->next;
  n->next = NULL;
  return n;
}

int check_correct(cube c)
{
  if(check_color(c,0,1,2,3)&&check_color(c,4,5,10,11)&&check_color(c,6,7,12,13)&&check_color(c,8,9,14,15)&&check_color(c,16,17,18,19)&&check_color(c,20,21,22,23))
	return 1;
  else return 0;
}

int check_color(cube c, int n1, int n2, int n3, int n4)
{
  if(c.c[n1]==c.c[n2] && c.c[n2]==c.c[n3] && c.c[n3]==c.c[n4])
	return 1;
  else return 0;
}

void print_solve(struct node* n)
{
  if(n->pre==NULL){
    cube_display(n->c);
    printf("\n");
    return;
  }else{
    print_solve(n->pre);
    printf("move %c%d\n", n->m.men, n->m.count);
    cube_display(n->c);
    printf("\n");
    return;
  }
}

struct node* cube_solver(cube c)
{
  move m;
  struct node* front, *rear, *n;
  int i, j, correct = 0;
  front = rear = init_queue();

  rear = enqueue(rear,NULL,c,m);
  while(1){
    if(front->next==NULL) break;
    if(front->next==rear) rear = front;
    n = dequeue(front);
    for(i=0;i<=2;i++){
      m.men = 'a'+i;
      if(m.men==n->m.men) continue; 
      for(j=1;j<=3;j++){
	m.count = j;
	c = cube_move(n->c,m);
        if(!(check_same(c,n))){
	  rear = enqueue(rear,n,c,m);
	  correct = check_correct(c);
        }
	if(correct) break;
      }
      if(correct) break;
    }
    if(correct) break;
  }
  return rear;
}

int check_same(cube c, struct node* n)
{
  int i, same;
  while(n!=NULL){
    for(i=0;i<=23;i++)
      if(c.c[i]!=n->c.c[i]) return 0;
    n = n->pre;
  }
  return 1;
}
