#include<stdio.h>
#include<stdlib.h>

#define N 1000000

/******prototypes******/
typedef struct {char c[26];} cube;
typedef struct {char men; int count;} move;
struct q_node{cube c; move m; struct q_node* next; struct q_node* pre;};
struct d_node{unsigned long long int c; struct d_node* next; struct d_node* pre;};

cube cube_init(cube c);
void cube_display(cube c);
move user(move m);
cube cube_move(cube c, move m);
struct q_node* init_queue();
struct q_node* enqueue(struct q_node* rear, struct q_node* pre, cube c, move m);
struct q_node* dequeue(struct q_node* front);
int check_correct(cube c);
int check_color(cube c, int n1, int n2, int n3, int n4);
void print_solve(struct q_node* n);
struct q_node* cube_solver(cube c);
int check_same(struct d_node* top, unsigned long long int hashc);
struct d_node* init_dlist();
void in_dlist(struct d_node* top, unsigned long long int c);
unsigned long long int hash(cube c);
int moved_num(struct q_node* n);

/******main******/
int main(){
  cube c;
  move m;
  struct q_node* solve;
  c = cube_init(c);
  //cube_display(c);

  solve = cube_solver(c);
  print_solve(solve);
  printf("%d\n", moved_num(solve));
  return 0;
}

/******function******/
cube cube_init(cube c)
{
  printf("Input initial cube\n");
  scanf("%26s", c.c);
  return c;
}

void cube_display(cube c)
{
  printf("  %c%c  \n", c.c[0], c.c[1]);
  printf("  %c%c  \n", c.c[2], c.c[3]);
  printf("%c%c%c%c%c%c\n", c.c[4], c.c[5], c.c[6], c.c[7], c.c[8], c.c[9]);
  printf("%c%c%c%c%c%c\n", c.c[10], c.c[11], c.c[12], c.c[13], c.c[14], c.c[15]);
  printf("  %c%c  \n", c.c[16], c.c[17]);
  printf("  %c%c  \n", c.c[18], c.c[19]);
  printf("  %c%c  \n", c.c[20], c.c[21]);
  printf("  %c%c  \n", c.c[22], c.c[23]);
}

move user(move m){
  char dummy;
  printf("回転面を入力[a,b,c]\n");
  scanf("%c", &m.men);
  printf("回転回数を入力[1~3]\n");
  scanf("%d", &m.count);
  scanf("%c", &dummy);

  return m;
}

cube cube_move(cube c, move m){
  int i;
  char temp[2];

  for(i=0; i<m.count; i++){//回転回数
    if(m.men == 'a'){
      temp[0] = c.c[4];
      temp[1] = c.c[5];
      c.c[4] = c.c[6];
      c.c[5] = c.c[7];
      c.c[6] = c.c[8];
      c.c[7] = c.c[9];
      c.c[8] = c.c[23];
      c.c[9] = c.c[22];
      c.c[23] = temp[0];
      c.c[22] = temp[1];
      temp[0] = c.c[0];
      c.c[0] = c.c[2];
      c.c[2] = c.c[3];
      c.c[3] = c.c[1];
      c.c[1] = temp[0];
    }else if(m.men == 'b'){
      temp[0] = c.c[1];
      temp[1] = c.c[3];
      c.c[1] = c.c[7];
      c.c[3] = c.c[13];
      c.c[7] = c.c[17];
      c.c[13] = c.c[19];
      c.c[17] = c.c[21];
      c.c[19] = c.c[23];
      c.c[21] = temp[0];
      c.c[23] = temp[1];
      temp[0] = c.c[8];
      c.c[8] = c.c[14];
      c.c[14] = c.c[15];
      c.c[15] = c.c[9];
      c.c[9] = temp[0];
    }else if(m.men == 'c'){
      temp[0] = c.c[0];
      temp[1] = c.c[1];
      c.c[0] = c.c[9];
      c.c[1] = c.c[15];
      c.c[9] = c.c[19];
      c.c[15] = c.c[18];
      c.c[19] = c.c[10];
      c.c[18] = c.c[4];
      c.c[10] = temp[0];
      c.c[4] = temp[1];
      temp[0] = c.c[22];
      c.c[22] = c.c[23];
      c.c[23] = c.c[21];
      c.c[21] = c.c[20];
      c.c[20] = temp[0];
    }
  }
  return c;
}

struct q_node* init_queue()
{
  struct q_node* n=(struct q_node*)malloc(sizeof(struct q_node));
  n->next = n->pre = NULL;
  return n;
}

struct q_node* enqueue(struct q_node* rear, struct q_node* pre, cube c, move m)
{
  struct q_node* n = init_queue();
  n->c = c;  n->m = m;  n->pre = pre;
  n->next = rear->next;  rear->next = n;
  return n;
}

struct q_node* dequeue(struct q_node* front)
{
  struct q_node* n = front->next;
  front->next = front->next->next;
  n->next = NULL;
  return n;
}

int check_correct(cube c)
{
  if(check_color(c,0,1,2,3)&&check_color(c,4,5,10,11)&&check_color(c,6,7,12,13)&&check_color(c,8,9,14,15)&&check_color(c,16,17,18,19)&&check_color(c,20,21,22,23))
	return 1;
  else return 0;
}

int check_color(cube c, int n1, int n2, int n3, int n4)
{
  if(c.c[n1]==c.c[n2] && c.c[n2]==c.c[n3] && c.c[n3]==c.c[n4])
	return 1;
  else return 0;
}

void print_solve(struct q_node* n)
{
  if(n->pre==NULL){
    cube_display(n->c);
    printf("\n");
    return;
  }else{
    print_solve(n->pre);
    printf("move %c%d\n", n->m.men, n->m.count);
    cube_display(n->c);
    //printf("%llu", hash(n->c));
    printf("\n");
    return;
  }
}

struct q_node* cube_solver(cube c)
{
int k=0;
  move m;
  struct q_node* front, *rear, *n ,*l, *max_min;
  int i, j, correct = 0, maxmin_movednum = 0, movednum;
  struct d_node* top[N];
  unsigned long long int hashc;
  front = rear = init_queue();

  for(i=0;i<N;i++) top[i] = init_dlist();

  rear = enqueue(rear,NULL,c,m);
  hashc = hash(c);
  in_dlist(top[hashc%N],hashc);
  while(1){
    if(front->next==NULL) break;
    if(front->next==rear) rear = front;
    n = dequeue(front);
    for(i=0;i<=2;i++){
      m.men = 'a'+i;
      //if(m.men==n->m.men) continue; 
      for(j=1;j<=3;j+=2){
	m.count = j;
	c = cube_move(n->c,m);
        hashc = hash(c);
        if(!(check_same(top[hashc%N],hashc))){
          rear = enqueue(rear,n,c,m);
          in_dlist(top[hashc%N],hashc);
          /*movednum = moved_num(front->next);
          if(movednum > maxmin_movednum){
            max_min = front->next;
            maxmin_movednum = movednum;
          }*/
          //printf("%d\n",k); 
          k++;
        }
      }
    }
  }
  //printf("%d\n",k);
  return n;
}

int check_same(struct d_node* top, unsigned long long int hashc)
{
  struct d_node *m = top->next, *n=top->pre;
  while(m->pre!=n){
    if(hashc==m->c || hashc==n->c) return 1;
    m = m->next;
    n = n->pre;
    if(m->pre->pre==n) break;
  }
  return 0;
}

struct d_node* init_dlist()
{
  struct d_node* n=(struct d_node*)malloc(sizeof(struct d_node));
  n->next=n;  n->pre=n;
  return n;
}

void in_dlist(struct d_node* top, unsigned long long int c)
{
  struct d_node* n=init_dlist();
  n->c=c;
  n->pre=top;  top->next->pre=n;
  n->next=top->next;  top->next=n;
  return;
}

unsigned long long int hash(cube c)
{
  unsigned long long int hashc=0, a;
  int i;
  for(i=0;i<24;i++){
    if(i==11 || i==12 || i==16) continue;
    if(c.c[i]=='w')      a=00;
    else if(c.c[i]=='r') a=01;
    else if(c.c[i]=='g') a=02;
    else if(c.c[i]=='o') a=03;
    else if(c.c[i]=='y') a=04;
    else                 a=05;
    hashc=(hashc<<3) | a;
  }
  return hashc;
}

int moved_num(struct q_node* n)
{
  if(n->pre==NULL) return 0;
  else{
    if(n->m.count==1 || n->m.count==3)
      return moved_num(n->pre)+1;
    else
      return moved_num(n->pre)+2;
  }
}
