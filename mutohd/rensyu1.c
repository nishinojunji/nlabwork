/* rensyu1.c : sum primes in [a:b] */
/* Kohsuke Mutoh (man0456yougray@gmail.com) */
/* 2016 */

#include<stdio.h>
#include<math.h>

typedef unsigned long long int ulli;

typedef struct{
  ulli a;
  ulli b;
} Inputs;


/* ----- prototypes ----- */
Inputs input();
int isPrime(ulli a);
ulli primeSum(ulli a, ulli b);
void output(ulli n);


/* ----- main ----- */
int main(void){
  Inputs in;
  ulli result;

  in = input();
  result = primeSum(in.a, in.b);
  output(result);
}


/* ----- functions ----- */
Inputs input(){
  Inputs in;
  char buf[80];

  fgets(buf, 80, stdin);
  sscanf(buf, "%lld %lld", &in.a, &in.b);

  return in;
}

int isPrime(ulli a){
  ulli i;
  float rta = sqrt(a);

  if(a<=1)
    return 0;
  for(i=3;i<=rta;i+=2){
    if(a%i == 0)
      return 0;
  }
  return 1;
}

ulli primeSum(ulli a, ulli b){
  ulli i;
  ulli sum;

  sum = 0;
  if(a<=2 && b>=2){
    sum += 2;
  }

  if(a%2 == 0)
    a += 1;
  
  for(i=a;i<=b;i+=2){
    if(isPrime(i)){
      sum += i;
    }
  }
  printf("\n");
  
  return sum;
}

void output(ulli n){
  char buf[80];

  sprintf(buf, "%lld\n", n);

  fputs(buf, stdout);
  
}
