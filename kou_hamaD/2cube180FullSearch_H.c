#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum{
  None,
  X,
  Y,
  Z,
} Dir;

typedef enum{
  _None,
  S0,
  X1,
  X2,
  X3,
  Y1,
  Y2,
  Y3,
  Z1,
  Z2,
  Z3,
} Dir_multi;

typedef struct{
  int color[6][4]; // [面の番号][ブロックの番号] -> 色の番号
                   // 0:Red 1:White 2:Blue 3:Yellow 4:Green 5:Orange
} Cube;

#define HistoryNum 128
Dir moveHistory[HistoryNum]; // [回転の順番] -> 回転の履歴 (3種)
Dir_multi moveHistoryMin[HistoryNum]; // [回転の順番] -> 回転の履歴 (9+1種)
Dir_multi subH[HistoryNum];
int minMove = 1023; // 最短経路
#define MaxMove 32

Cube init_cube();
void display_cube(Cube c);
void display_face(Cube c, int i);
char* str_color(int color);
Cube move(Cube c, int dir);

Cube edit_cube(Cube c); // キューブをコマンド入力して回転操作
Cube input_cube(Cube c); // キューブを1つずつ入力
Cube input_cube_str(Cube c, char* input); // input_cubeの文字列制御版
int color_mode = 1; // 色付きモード

void init_moveHistory(); // 回転履歴の初期化
int arrange_moveHistory(int i); // 経路の整理 -> 手数
int bayoeen(); // 配列の中身の整理用
void commit_moveHistory(); // 見つけた最小経路の登録
void display_moveHistory(int i); // 回転履歴の閲覧
Cube shuffle_cube(Cube c); // シャッフル
void search_6men(Cube c); // 探索
int is_completion(Cube c); // 完成判定

// 幅優先探索用
#define SIZE_LIST 4000000
typedef long long unsigned Hash;
typedef struct 
{
  Cube c;
  int move;
  int parent_node;
} Node;
Hash convert(Cube c);
void add_hash(Hash list[], Hash h, int* tail_hash);
void breadthFirstSearch(Cube c);
void print_solution(Node list[], int sol_node);
void push_list(Node list[], int* tail_list, Cube c_new, int move, int head_list);
Node pop_list(Node list[], int head_list);
int isPresent(Hash list[], Hash h, int tail_hash);
char* move2str(int move);
Hash visited[SIZE_LIST];
Node openlist[SIZE_LIST];

// モンテカルロ用
void monte_decideMove();

int main(int argc, char const *argv[])
{
  Cube c = init_cube();
  char command;
  srand((unsigned)time(NULL));
  color_mode = 1;

  display_cube(c = init_cube());
  breadthFirstSearch(c);  

  return 0;
}

Cube init_cube()
{
  Cube c;
  int i,j;
  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      c.color[i][j] = i;
    }
  }
  return c;
}

void display_cube(Cube c)
{
  int i,j;

  char* cc[6][4];
  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      cc[i][j] = str_color(c.color[i][j]);
    }
  }
  printf("==================="    "   ＿＿＿＿＿"     "       \t ＿＿＿＿＿\n");

  printf("| %s %s | %s %s | %s %s |",cc[0][0], cc[0][1], cc[1][0], cc[1][1], cc[5][0], cc[5][1]);
  printf("  |＼  %s  %s  ＼", cc[1][0], cc[1][1]);
  printf("     \t|          |＼\n");
  printf("| %s %s | %s %s | %s %s |",cc[0][2], cc[0][3], cc[1][2], cc[1][3], cc[5][2], cc[5][3]);
  printf("  |  ＼  %s  %s  ＼",cc[1][2], cc[1][3]);  
  printf("   \t|   %s  %s   |  ＼\n", cc[4][2], cc[4][3]);

  printf("===================");
  printf("  | %s  |￣￣￣￣|", cc[0][1]);
  printf("   \t|          | %s  |\n", cc[5][0]);

  printf("      | %s %s |", cc[2][0], cc[2][1]);
  printf("        |  %s |  %s  %s  |", cc[0][3], cc[2][0], cc[2][1]);
  printf( "   \t|          |  %s |\n", cc[5][2]);
  printf("      | %s %s |", cc[2][2], cc[2][3]);
  printf("        | %s  |        |" , cc[0][0]);
  printf( "   \t|   %s  %s   | %s  |\n", cc[4][0], cc[4][1], cc[5][1]);

  printf("      =======");
  printf("        |  %s |        |", cc[0][2]);
  printf( "   \t|＿＿＿＿＿|  %s |\n", cc[5][3]);

  printf("      | %s %s |", cc[3][0], cc[3][1]);
  printf("         ＼  |  %s  %s  |", cc[2][2], cc[2][3]);
  printf("   \t ＼  %s  %s   ＼  |   \n", cc[3][2], cc[3][3]);
  printf("      | %s %s |", cc[3][2], cc[3][3]);
  printf("           ＼|＿＿＿＿|");
  printf("   \t   ＼  %s  %s   ＼|       \n", cc[3][0], cc[3][1]);

  printf("      ======="           "                                 ￣￣￣￣￣\n");

  printf("      | %s %s |\n", cc[4][0], cc[4][1]);
  printf("      | %s %s |\n", cc[4][2], cc[4][3]);

  printf("      =======      \n");
  
  return;
  //         | 40 41 |
  //         | 42 43 |

  // | 00 01 | 10 11 | 50 51 | 33 32 |
  // | 02 03 | 12 13 | 52 53 | 31 30 |

  //         | 20 21 |
  //         | 22 23 |

  //         | 30 31 |
  //         | 32 33 |

  //         | 40 41 |
  //         | 42 43 |
}

char* str_color(int color)
{
  // グローバルの color_mode を使用しているので注意
  if (color_mode)
    {
      switch (color) 
  {
  case -1:
    return "+";
    break;
  case 0:
    // Red
    return "\x1b[31m\x1b[41mR\x1b[49m\x1b[39m";
    break;
  case 1:
    // White
    return "\x1b[37m\x1b[47mW\x1b[49m\x1b[39m";
    break;
  case 2:
    // Blue
    return "\x1b[34m\x1b[44mB\x1b[49m\x1b[39m";
    break;
  case 3:
    // Yellow
    return "\x1b[33m\x1b[43mY\x1b[49m\x1b[39m";
    break;
  case 4:
    // Green
    return "\x1b[32m\x1b[42mG\x1b[49m\x1b[39m";
    break;
  case 5:
    // Orange
    return "\x1b[36m\x1b[46mO\x1b[49m\x1b[39m";
    break;
  default :
    return " ";
    break;
  }
    }
  else
    {
      switch (color) 
  {
  case -1:
    return "+";
    break;
  case 0:
    // Red
    return "R";
    break;
  case 1:
    // White
    return "W";
    break;
  case 2:
    // Blue
    return "B";
    break;
  case 3:
    // Yellow
    return "Y";
    break;
  case 4:
    // Green
    return "G";
    break;
  case 5:
    // Orange
    return "O";
    break;
  default :
    return " ";
    break;
  }
    }
}

Cube move(Cube c, int dir)
{
  Cube moved = c;
  switch (dir) 
    {
    case X:
      // 回転軸に平行な面(面1,2,3,4)の回転
      moved.color[1][1] = c.color[2][1];
      moved.color[1][3] = c.color[2][3];

      moved.color[2][1] = c.color[3][1];
      moved.color[2][3] = c.color[3][3];

      moved.color[3][1] = c.color[4][1];
      moved.color[3][3] = c.color[4][3];

      moved.color[4][1] = c.color[1][1];
      moved.color[4][3] = c.color[1][3];

      // 回転軸に垂直な面(面5)の回転
      moved.color[5][0] = c.color[5][2];
      moved.color[5][2] = c.color[5][3];
      moved.color[5][3] = c.color[5][1];
      moved.color[5][1] = c.color[5][0];

      break;
    case Y:
      // 回転軸に平行な面(面0,2,5,4)の回転
      moved.color[0][0] = c.color[2][2];
      moved.color[0][2] = c.color[2][3];

      moved.color[2][2] = c.color[5][3];
      moved.color[2][3] = c.color[5][1];

      moved.color[5][3] = c.color[4][1];
      moved.color[5][1] = c.color[4][0];

      moved.color[4][1] = c.color[0][0];
      moved.color[4][0] = c.color[0][2];

      // 回転軸に垂直な面(面3)の回転
      moved.color[3][0] = c.color[3][1];
      moved.color[3][2] = c.color[3][0];
      moved.color[3][3] = c.color[3][2];
      moved.color[3][1] = c.color[3][3];

      break;
    case Z:
      // 回転軸に平行な面(面1,5,3,0)の回転
      moved.color[1][0] = c.color[5][0];
      moved.color[1][1] = c.color[5][1];

      moved.color[5][0] = c.color[3][3];
      moved.color[5][1] = c.color[3][2];

      moved.color[3][3] = c.color[0][0];
      moved.color[3][2] = c.color[0][1];

      moved.color[0][0] = c.color[1][0];
      moved.color[0][1] = c.color[1][1];

      // 回転軸に垂直な面(面4)の回転
      moved.color[4][0] = c.color[4][2];
      moved.color[4][2] = c.color[4][3];
      moved.color[4][3] = c.color[4][1];
      moved.color[4][1] = c.color[4][0];
    
      break;
    default:
      printf("unexpected move\n");
      break;
    }
  return moved;
}

Cube edit_cube(Cube c)
{
  int roop = 1;
  char command;
  while ( roop )
  {
    printf(" ---- x,y,zで回転, qで終了 ---- \n");
    scanf("\n%c", &command);
    switch (command)
    {
    case 'x':
      c = move(c, X);
      break;
    case 'y':
      c = move(c, Y);
      break;
    case 'z':
      c = move(c, Z);
      break;
    case 'q':
      roop = 0;
      break;
    }
    display_cube(c);
  }
  return c;
}

Cube input_cube(Cube c)
{
  int i,j;
  Cube c_out;
  char command;

  // 空白で初期化
  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      c_out.color[i][j] = 6;
    }
  }

  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      c_out.color[i][j] = -1;
      display_cube(c_out);
      printf(" ---- 0-5 または r w b y g o で + に色を入力, cでキャンセル, qで終了 ---- \n");
    retry:
      scanf("\n%c", &command);
      switch (command) 
      {
      case '0': case 'r':
        // Red
        c_out.color[i][j] = 0;
        break;
      case '1': case 'w':
        // White
        c_out.color[i][j] = 1;
        break;
      case '2': case 'b':
        // Blue
        c_out.color[i][j] = 2;
        break;
      case '3': case 'y':
        // Yellow
        c_out.color[i][j] = 3;
        break;
      case '4': case 'g':
        // Green
        c_out.color[i][j] = 4;
        break;
      case '5': case 'o':
        // Orange
        c_out.color[i][j] = 5;
        break;
      case 'c':
        goto input_cancel;
        break;
      case 'q':
        goto input_quit;
        break;
      default :
        goto retry;
        break;
      }
    }
  }
  input_quit:
  c = c_out;
  input_cancel:
  display_cube(c);
  return c;
}

Cube input_cube_str(Cube c, char* input)
{
  int i,j;
  Cube c_out;
  char command;
  char* p = input; // input = "000011112222333344445555q"ならデフォルト

  // 空白で初期化
  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      c_out.color[i][j] = 6;
    }
  }

  for (i=0;i<6;i++) {
    for (j=0;j<4;j++) {
      c_out.color[i][j] = -1;
    retry:
      command = *p;
      p++;

      switch (command) 
      {
      case '0': case 'r':
        // Red
        c_out.color[i][j] = 0;
        break;
      case '1': case 'w':
        // White
        c_out.color[i][j] = 1;
        break;
      case '2': case 'b':
        // Blue
        c_out.color[i][j] = 2;
        break;
      case '3': case 'y':
        // Yellow
        c_out.color[i][j] = 3;
        break;
      case '4': case 'g':
        // Green
        c_out.color[i][j] = 4;
        break;
      case '5': case 'o':
        // Orange
        c_out.color[i][j] = 5;
        break;
      case 'c':
        goto input_cancel;
        break;
      case 'q': case '\0':
        goto input_quit;
        break;
      default :
        goto retry;
        break;
      }
    }
  }
  input_quit:
  c = c_out;
  input_cancel:
  display_cube(c);
  return c;
}


void init_moveHistory() // 手順履歴の初期化
{
  int i;
  for (i=0;i<HistoryNum;i++) {
    moveHistory[i] = None;
    moveHistoryMin[i] = None;
  }
  return;
}

int arrange_moveHistory(int i)
{
  int j,k,l;
  Dir dir;

  for (j=i+1;j<HistoryNum;j++) moveHistory[j] = None;

  l = bayoeen();
  
  return l;
}

int bayoeen() // 配列の中身の整理用
{
  int i,j,k,l;
  Dir dir;
  
  dir = moveHistory[0];
  k=1;
  
  for (j=1,l=0;1;j++) {
    if (dir==moveHistory[j] && k % 4!=0) {
      k++;
      continue;
    } 

    if (k % 4==0) {
      for (i=j;moveHistory[i]!=None;i++) {
  moveHistory[i-4] = moveHistory[i];
      }
      moveHistory[i-4] = None;
      moveHistory[i-3] = None;
      moveHistory[i-2] = None;
      moveHistory[i-1] = None;
      return bayoeen();
      
    } else subH[l] = (moveHistory[j-1] * 3 - 1) + ((k - 1) % 4);
    l++;
    
    if (moveHistory[j]==None) break;
    
    dir = moveHistory[j];
    k=1;
  }

  return l;
}

void commit_moveHistory()
{
  int i;
  for (i=0;i<HistoryNum;i++) moveHistoryMin[i] = subH[i];
  return;
}

void display_moveHistory(int i) // 回転履歴の閲覧
{
  int j;
  
  for (j=0;j<HistoryNum;j++) {
    if (i==0) {
      if (moveHistoryMin[j]==_None) break;
      switch (moveHistoryMin[j]) {
      case S0:
  printf("S0 ");
  break;
      case X1:
  printf("X1 ");
  break;
      case X2:
  printf("X2 ");
  break;
      case X3:
  printf("X! ");
  break;
      case Y1:
  printf("Y1 ");
  break;
      case Y2:
  printf("Y2 ");
  break;
      case Y3:
  printf("Y! ");
  break;
      case Z1:
  printf("Z1 ");
  break;
      case Z2:
  printf("Z2 ");
  break;
      case Z3:
  printf("Z! ");
  break;
      default:
  break;
      }

      
    } else if (i==1) {
      if (moveHistory[i]==None) break;
      switch (moveHistory[i]) {
      case X:
  printf("X ");
  break;
      case Y:
  printf("Y ");
  break;
      case Z:
  printf("Z ");
  break;
      default:
  printf("error: display_moveHistory() -> moveHistory[%d]\n",i);
  break;
      }
    }
  }
  printf("= \n");
}

Cube shuffle_cube(Cube c) // シャッフル
{
  int n,i;
  Cube c0;
  n = rand()%8 + 8;
  c0 = c;

  printf("--- shuffle_cube() begin\n");
  for (;n>0;n--) {
    i = rand()%3;
    switch (i)
      {
      case 0:
  c0 = move(c0,X); // X軸回転
  printf("[X ");
  break;
      case 1:
  c0 = move(c0,Y); // Y軸回転
  printf("[Y ");
  break;
      case 2:
  c0 = move(c0,Z); // Z軸回転
  printf("[Z ");
  break;
      default:
  printf("error: shuffle_cube() -> i\n");
  exit(1);
      }
  }
  printf("\n--- shuffle_cube() end\n");
  
  return c0;
}


//-----------------------------
// 方針
// 1- ランダムに回しまくって、完成を目指す。
//    8手以内で必ず完成するとかなので、
//    8手ランダムで回しても完成しなかった場合、
//    最初からやり直し。
//    いわゆる、「原始モンテカルロ法」。
//
// 2- 回し得る全ての行動を試しまくって、完成を目指す。
//    X軸回転が90. 180. 270. の各3通り、X,Y,Z軸で全9通り。
//    一手回したら、次は同じ軸を選択しないようにする。
//    で、高々9*(6^7)通りを試す。
//    いわゆる、「全探索」。
//
// 3- 上記の方法に関連して、知識ベースの探索を適用して、
//    完成を目指す。
//    最適でない操作ってのが事前に分かっていれば、
//    その操作を避けて探索を行えればいいなって。
//    いわゆる「ヒューリスティック探索」。
//    =====  却　下　=====
//
// 4- 「幅優先探索」
//    open listを使って、状態保存。
//    色配置と、手順を保存？
//    既に同じ状態になったら、listから消去。
//    s = openlist.pop;
//    if (is(hash,s)) continue;
//    ms = moves(s);
//    openlist.add(ms);
//    
//
//-----------------------------
void search_6men(Cube c) // 探索
{
  int i,j,flagB=0;
  long counter;
  Cube sub_c;
  printf("--- search_6men() begin\n");

  if (is_completion(c)) { // 既に完成してたら、
    printf("=== finished without move\n");
    return;
  }
  
  for (counter=0;counter<1145140;counter++) {
    // loop begin
    sub_c=c;
    
    // 回し方を決める
    monte_decideMove();

    // 回す
    for (i=0;i<32;i++) {
      sub_c = move(sub_c,moveHistory[i]);
      
      if (is_completion(sub_c)) { // 完成したら、
  i = arrange_moveHistory(i);
  printf("=== found [%02d] --%07ld\n",i,counter);
  if (minMove>i) {
    minMove = i;
    commit_moveHistory();
  }
  break;
      }
    }  
  }

  if (minMove==1023){
    printf("=== not found\n");
    exit(1);
  }
  
  display_moveHistory(0);
  for (j=1;j<minMove;j++) printf("   ");
  printf("!\n");
  printf("[X1 : X方向に90.] [X2 : X方向に180.] [X! : 逆X方向に90.]\n");
  
  printf("\n--- search_6men() end\n");

  return;
}

int is_completion(Cube c) // 完成判定
{
  int i,j;

  for (i=0;i<5;i++) { // 5面まで完成してるか見てる
    for (j=1;j<4;j++) {
      if (c.color[i][0] != c.color[i][j]) return 0;
    }
  }

  return 1;
}

void monte_decideMove()
{
  int i,j;
  for (i=0;i<MaxMove;i++) {
    j = rand()%3+1;
    moveHistory[i] = j;
  }
}

Hash convert(Cube c)
{
  Hash hash = 0, temp;
  int i, j, dig = 0;

  for (i = 0; i < 6; ++i)
  {
    for (j = 0; j < 4; ++j)
    {
      // サイズ節約のため固定ブロックはハッシュ化に考慮しない
      if (1
          && !(i == 0 && j == 3)
          && !(i == 1 && j == 2)
          && !(i == 2 && j == 0))
      {
        temp = c.color[i][j];       // ビットシフトできるようintからunsigned long long intに値を保存
        hash |= (temp << (dig*3));  // 3ビットで1色を表現
        ++dig;                      // 次に記録する桁を1つずらす
      }
    }
  }

  return hash;
}
void add_hash(Hash list[], Hash h, int* tail_hash)
{
  // -------- ソート無し --------
  // list[*tail_hash] = h;
  // ------------------------

  // -------- 昇順ソート --------
  int i, j;
  // 挿入位置をiに保存
  for (i = 0; i < *tail_hash; ++i)
  {
    if (h < list[i])
    {
      break;
    }
  }
  *tail_hash += 1;
  // 後ろから挿入位置までリストをずらす
  for (j = *tail_hash; j > i; --j)
  {
    list[j] = list[j-1];
  }
  // 挿入
  list[j] = h;
  // ------------------------
}
int isPresent(Hash list[], Hash h, int tail_hash)
{
  // -------- 線形探索 --------
  // int i;
  // for (i = 0; i < tail_hash; ++i)
  // {
  //   if (list[i] == h)
  //   {
  //     return 1;
  //   }
  // }
  // ------------------------

  // -------- 二分探索 --------
  int low = 0;
  int high = tail_hash - 1;
  int middle;

  while (low <= high)
  {
    middle = (low + high) / 2;
    if (h == list[middle])
    {
      return 1;
    }
    else if (h < list[middle])
    {
      high = middle - 1;
    }
    else if (h > list[middle])
    {
      low = middle + 1;
    }
  }
  // ------------------------

  return 0;
}
void breadthFirstSearch(Cube c)
{
  Hash temp_hash;
  Node temp_node;
  Cube temp_cube;
  int i;
  int tail_hash = 0; // ハッシュリストの末尾への添字
  int head_list = 0; // 探索リストの先頭(探索中のノード)への添字
  int tail_list = 0; // 探索リストの末尾への添字
  int opened = 0;    // 展開ノード数表示用

  // 探索リストの先頭に解きたいキューブを追加
  push_list(openlist, &tail_list, c, 0, head_list);

  printf("ノード 探索数 / 展開数\n");
  for (head_list = 0; head_list <= tail_list; head_list++)
  {
    // 次に探索するノードを取り出す
    temp_node = pop_list(openlist, head_list);

    if (tail_list == head_list)
    {
      // 取り出したノードが完成しているなら手順を表示
      printf("\r %d / %d\n", head_list, tail_list);
      display_cube(openlist[tail_list - 1].c);
      break;
    }
    else
    {
      // X,Y,Z方向の3回転とその逆の3回転をそれぞれについて
      for (i = 1; i <= 6; ++i)
      {
        switch (i)
        {
        case 1:
          temp_cube = move(temp_node.c, X);
          break;
        case 2:
          temp_cube = move(temp_node.c, Y);
          break;
        case 3:
          temp_cube = move(temp_node.c, Z);
          break;
        case 4:
          temp_cube = move(temp_node.c, X);
          temp_cube = move(temp_cube, X);
          temp_cube = move(temp_cube, X);
          break;
        case 5:
          temp_cube = move(temp_node.c, Y);
          temp_cube = move(temp_cube, Y);
          temp_cube = move(temp_cube, Y);
          break;
        case 6:
          temp_cube = move(temp_node.c, Z);
          temp_cube = move(temp_cube, Z);
          temp_cube = move(temp_cube, Z);
          break;
        case 7:
          temp_cube = move(temp_node.c, X);
          temp_cube = move(temp_cube, X);
          break;
        case 8:
          temp_cube = move(temp_node.c, Y);
          temp_cube = move(temp_cube, Y);
          break;
        case 9:
          temp_cube = move(temp_node.c, Z);
          temp_cube = move(temp_cube, Z);
          break;

        }
        
        // ハッシュリストに存在していない場合にのみ
        // 探索リストに追加し、ハッシュリストも更新する
        if (!isPresent(visited, temp_hash = convert(temp_cube), tail_hash))
        {
          push_list(openlist, &tail_list, temp_cube, i, head_list);
          add_hash(visited, temp_hash, &tail_hash);
        }
      }
      if (opened < tail_list / 1000)
      {
        opened = tail_list / 1000;
        printf("\r %d / %d", head_list, tail_list);
        fflush(stdout);
      }
    }
  }

  // for (i = 0; i < tail_hash; ++i)
  // {
  //   printf("%Lo\n", visited[i], visited[i]);
  // }
}
void push_list(Node list[], int* tail_list, Cube c_new, int move, int head_list)
{
  Node n_new;
  n_new.c = c_new;
  n_new.move = move;
  n_new.parent_node = head_list;
  list[*tail_list] = n_new;
  *tail_list += 1;
}
Node pop_list(Node list[], int head_list)
{
  Node n = list[head_list];
  return n;
}
void print_solution(Node list[], int sol_node)
{
  int temp = sol_node;
  static int count = 0;

  if (list[temp].move == 0)
  {
    return;
  }
  else
  {
    print_solution(list, list[temp].parent_node);
    printf("%d %s\n", ++count, move2str(list[temp].move));
    return;
  }

  // printf("解答手順(下から順)\n");
  // for (temp = sol_node; list[temp].move != 0; temp = list[temp].parent_node, count++)
  // {
  //   // printf("%d \n", list[temp].move);
  //   printf("%s\n", move2str(list[temp].move));
  // }
  // printf("↑ 以上の%d手順\n", count);
}
char* move2str(int move)
{
  switch (move)
  {
  case 1:
    return "X方向に+ 90°";
    break;
  case 2:
    return "Y方向に+ 90°";
    break;
  case 3:
    return "Z方向に+ 90°";
    break;
  case 4:
    return "X方向に- 90°";
    break;
  case 5:
    return "Y方向に- 90°";
    break;
  case 6:
    return "Z方向に- 90°";
    break;
  case 7:
    return "X方向に+180°";
    break;
  case 8:
    return "Y方向に+180°";
    break;
  case 9:
    return "Z方向に+180°";
    break;
  default:
    return "???";
    break;
  }  
}
