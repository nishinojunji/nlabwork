#include<stdio.h>
#include<math.h>

/***** prototypes *****/
typedef struct {unsigned long long s;} Input;

unsigned long long prime_sum(unsigned long long);
int is_prime(unsigned long long);
Input input();

/***** main *****/
void main(){
  unsigned long long sum=0;

  //入力
  Input a;
  a = input();

  //処理
  sum = prime_sum(a.s);

  //出力
  printf("sum of primes [%llu]\n", sum);
}

/***** function *****/
unsigned long long prime_sum(unsigned long long a){
  unsigned long long i=0, sum=0;

  for(i=a; i<=a+1000; i++){
    if(is_prime(i)==1){
      sum += i;
      //printf("%llu += %llu\n", sum, i);
    }
  }

  return sum;
}

int is_prime(unsigned long long i){
  unsigned long long k=0;
  if(i%2==0) return 0;
  for(k=3; k*k<i; k+=2){
    if(i%k==0) return 0;
  }
  return 1;
}

Input input(){
  Input a;

  scanf("%llu",&a.s);

  return a;
}

