#include<stdio.h>

/***** prototypes *****/
typedef struct {int s, e;} Input;

int prime_sum(int,int);
int is_prime(int);
Input input();

/***** main *****/
void main(){
  int sum=0;

  //入力
  Input a;
  a = input();

  //処理
  sum = prime_sum(a.s, a.e);

  //出力
  printf("sum of primes [%d]\n", sum);
}

/***** function *****/
int prime_sum(int a, int b){
  int i=0, sum=0;

  for(i=a; i<=b; i++){
    if(is_prime(i)==1){
      sum += i;
    }
  }

  return sum;
}

int is_prime(int i){
  int k=0;
  if(i%2==0) return 0;
  for(k=3; k<(i/2); k+=2){
    if(i%k==0) return 0;
  }
  return 1;
}

Input input(){
  Input a;

  scanf("%d",&a.s);
  scanf("%d",&a.e);

  return a;
}

