#include<stdio.h>
int calc_sum(int a,int b);
int check_sosu(int num);
// typedef ..{s, e} Inputs; 構造体にしよう
typedef struct{
  int s;
  int e;
} Inputs;
Inputs input();
void output(int result,Inputs in);
/*-------------------prottype-----------------------*/

int main(){
  int result;
  Inputs in;
  in = input();
  result = calc_sum(in.s, in.e);
  output(result,in);
}

/*--------------------function----------------------*/
Inputs input(){
  Inputs in;
  printf("範囲を決定[a:b]\n");
  printf("a:");
  scanf("%d",&in.s);
  printf("b:");
  scanf("%d",&in.e);
  return in;
}

void output(int result,Inputs in){
  printf("%dから%dまでの素数の合計は%dです\n",in.s,in.e,result);
}

int calc_sum(int a,int b){
  int sum=0,i,tmp_a=a;
  if(a==0 || a==1){sum=2; tmp_a=3;}
  else if(a==2){sum=2; tmp_a=3;}
  else if(a%2==0){tmp_a+=1;}
  // ↓ここは１行にするのはやめよう
  for(i=tmp_a;i<b+1;i+=2) {
    if(check_sosu(i)==1) sum += i;
  }
  return sum;
}

//素数なら1、それ以外は−1を返す
int check_sosu(int num){
  int i;
  for(i=3;i<(num/2+1);i+=2){
    if(num%i==0 && i!=num) return -1;
  }
  return 1;
}
