//sosu2.c
//sadose kan
//2016_4_13

#include<stdio.h>
#include<math.h>
// typedef ..{s, e} Inputs; 構造体にしよう
typedef struct{
  double s; 
  double e;
} Inputs;
Inputs input();
void output(double result,Inputs in);
int check_sosu(double num);
double calc_sum(Inputs in);
/*-------------------prottype-----------------------*/

int main(){
  double result;
  Inputs in;
  in = input();
  result = calc_sum(in);
  output(result,in);
}

/*--------------------function----------------------*/
Inputs input(){
  Inputs in;
  printf("範囲を決定[a:b]\n");
  printf("a:");
  scanf("%lf",&in.s);
  printf("b:");
  scanf("%lf",&in.e);
  return in;
}

void output(double result,Inputs in){
  printf("%8.0fから%8.0fまでの素数の合計は%20.0fです\n",in.s,in.e,result);
}

double calc_sum(Inputs in){
  double sum=0.0,tmp_a=in.s;
  double i;
  if(tmp_a<2){sum=2.0; tmp_a=3.0;}
  else if(tmp_a<3){sum=2.0; tmp_a=3.0;}
  else if(fmod(tmp_a,2.0)<=0.0000001){tmp_a+=1.0;}
  // ↓ここは１行にするのはやめよう
  for(i=tmp_a;i<in.e;i+=2.0) {
    if(check_sosu(i)==1) {
      sum += i;
      //printf("%f\n",i);  //確認用
    }
  }
  return sum;
}

//素数なら1、それ以外は−1を返す
int check_sosu(double num){
  double i;
  for(i=3.0;i<sqrt(num)+1.0;i+=2.0){
    if(fmod(num,i)<=0.00001 && i!=num) return -1;
  }
  return 1;
}
