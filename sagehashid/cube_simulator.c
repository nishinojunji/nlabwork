/* cube_simulator.c : 2x2 Rubik's cube simulator */
/* Rin Sagehashi */
/* 2016/4/14 */

typedef struct {
  char  block_color[6]; // 使う色
  char  color[6][4]; // [面][ブロック]
} Cube;

typedef int Move;


/* ---- prototype ---- */

Cube cube_init();
void cube_display(Cube c);
Move user();
Cube move(Cube c, Move m);


/* ---- main ---- */

int main(int argc, char* argv[]) {
  Cube c;
  Move m;

  c = cube_init();

  while(1) {
    cube_display(c);
    m = user();
    c = move(c, m);
  }
}

/* ---- function ---- */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

Cube cube_randomize(Cube c) {
  int i, new, old;
  Move mv[9] = {11,21,31,12,22,32,13,23,33};
  FILE* fp;

  fp = fopen("output.txt", "w+");
  srand((unsigned) time(NULL));
  new = rand();
  c = move(c, mv[new % 9]);
  fprintf(fp, "%d ", mv[new % 9]);
  for(i=1; i<1000; i++) {
    old = new;
    do {
      new = rand();
    } while(new % 3 == old % 3);
    c = move(c, mv[new % 9]);
    fprintf(fp, "%d ", mv[new % 9]);
  }
  fclose(fp);
  return c;
}

Cube cube_init() {
  Cube c;
  int i, j;
  FILE *fp;
  char buf[1000];

  c.block_color[0] = 'w';
  c.block_color[1] = 'r';
  c.block_color[2] = 'b';
  c.block_color[3] = 'o';
  c.block_color[4] = 'g';
  c.block_color[5] = 'y';

  for(i=0; i<6; i++) {
    for(j=0; j<4; j++) {
      //c.color[i][j] = c.block_color[i];
      c.color[i][j] = i;
    }
  }

  c = cube_randomize(c);

  return c;
}
/*
  |g|
|r|w|o|y|
  |b|
*/



void cube_display(Cube c) {
  int i, j;
  char* col5[6];
  char* col2[6];
  char* col1[6];

  col5[0] = "\e[37;47mWWWWW\e[49;39m";
  col5[1] = "\e[31;41mRRRRR\e[49;39m";
  col5[2] = "\e[38;5;19;48;5;19mBBBBB\e[49;39m";
  col5[3] = "\e[38;5;202;48;5;202mOOOOO\e[49;39m";
  col5[4] = "\e[38;5;22;48;5;22mGGGGG\e[49;39m";
  col5[5] = "\e[33;43mYYYYY\e[49;39m";

  col2[0] = "\e[37;47mWW\e[49;39m";
  col2[1] = "\e[31;41mRR\e[49;39m";
  col2[2] = "\e[38;5;19;48;5;19mBB\e[49;39m";
  col2[3] = "\e[38;5;202;48;5;202mOO\e[49;39m";
  col2[4] = "\e[38;5;22;48;5;22mGG\e[49;39m";
  col2[5] = "\e[33;43mYY\e[49;39m";

  col1[0] = "\e[37;47mW\e[49;39m";
  col1[1] = "\e[31;41mR\e[49;39m";
  col1[2] = "\e[38;5;19;48;5;19mB\e[49;39m";
  col1[3] = "\e[38;5;202;48;5;202mO\e[49;39m";
  col1[4] = "\e[38;5;22;48;5;22mG\e[49;39m";
  col1[5] = "\e[33;43mY\e[49;39m";

  system("clear");
  printf("                   +-----+-----+++-----+-----+\n");
  printf("                  /%s/%s//||%s|%s|\n", col5[c.color[4][0]], col5[c.color[4][3]], col5[c.color[1][2]], col5[c.color[1][1]]);
  printf("                 /%s/%s//%s||%s|%s|\n", col5[c.color[4][0]], col5[c.color[4][3]], col1[c.color[5][2]], col5[c.color[1][2]], col5[c.color[1][1]]);
  printf("                +-----+-----++%s++-----+-----+\n", col2[c.color[5][2]]);
  printf("               /%s/%s//|%s/||%s|%s|\n", col5[c.color[4][1]], col5[c.color[4][2]], col1[c.color[5][2]], col5[c.color[1][3]], col5[c.color[1][0]]);
  printf("              /%s/%s//%s|/%s||%s|%s|\n", col5[c.color[4][1]], col5[c.color[4][2]], col1[c.color[5][3]], col1[c.color[5][1]], col5[c.color[1][3]], col5[c.color[1][0]]);
  printf("+-----+-----++=====+=====++%s+%s++-----+-----+\n", col2[c.color[5][3]], col2[c.color[5][1]]);
  printf("|%s|%s||%s|%s||%s/|%s/\n", col5[c.color[0][3]], col5[c.color[0][2]], col5[c.color[3][3]], col5[c.color[3][2]], col1[c.color[5][3]], col1[c.color[5][1]]);
  printf("|%s|%s||%s|%s||/%s|/\n", col5[c.color[0][3]], col5[c.color[0][2]], col5[c.color[3][3]], col5[c.color[3][2]], col1[c.color[5][0]]);
  printf("+-----+-----++-----+-----++%s+\n", col2[c.color[5][0]]);
  printf("|%s|%s||%s|%s||%s/\n", col5[c.color[0][0]], col5[c.color[0][1]], col5[c.color[3][0]], col5[c.color[3][1]], col1[c.color[5][0]]);
  printf("|%s|%s||%s|%s||/\n", col5[c.color[0][0]], col5[c.color[0][1]], col5[c.color[3][0]], col5[c.color[3][1]]);
  printf("+-----+-----++=====+=====++\n");
  printf("             |%s|%s|\n", col5[c.color[2][3]], col5[c.color[2][2]]);
  printf("             |%s|%s|\n", col5[c.color[2][3]], col5[c.color[2][2]]);
  printf("             +-----+-----+\n");
  printf("             |%s|%s|\n", col5[c.color[2][0]], col5[c.color[2][1]]);
  printf("             |%s|%s|\n", col5[c.color[2][0]], col5[c.color[2][1]]);
  printf("             +-----+-----+\n");
}
/*
      |40|43|
      |41|42|
|03|02|33|32|53|52|12|11|
|00|01|30|31|50|51|13|10|
      |23|22|
      |20|21|
*/
/*

               +----+----+++----+----+
              /gggg/gggg//||rrrr|rrrr|
             +----+----++y++----+----+
            /gggg/gggg//|/||rrrr|rrrr|
+----+----++====+====++y+y++----+----+
|wwww|wwww||oooo|oooo||/|/
+----+----++----+----++y+
|wwww|wwww||oooo|oooo||/
+----+----++====+====++
           |bbbb|bbbb|
           +----+----+
           |bbbb|bbbb|
           +----+----+
*/

Move user() {
  Move m;
  char buf[80];
  
  fgets(buf, 80, stdin);
  sscanf(buf, "%d", &m);

  return m;
}
void rotate4(char* a, char* b, char* c, char* d) {
  char old_a=*a, old_b=*b, old_c=*c, old_d=*d;
  *a=old_d;
  *b=old_a;
  *c=old_b;
  *d=old_c;
}
void rotate8(char* a, char* b, char* c, char* d ,char* e ,char* f ,char* g,char* h) {
  char old_a=*a, old_b=*b, old_c=*c, old_d=*d, old_e=*e, old_f=*f, old_g=*g, old_h=*h;
  *a=old_g;
  *b=old_h;
  *c=old_a;
  *d=old_b;
  *e=old_c;
  *f=old_d;
  *g=old_e;
  *h=old_f;
}

/*
      |43|42|
      |40|41|
|12|11|03|02|33|32|53|52|
|13|10|00|01|30|31|50|51|
      |20|23|
      |21|22|
*/	
Cube move(Cube c, Move m) {
  int i, j, k;

  i = m / 10;
  if(i < 1 && i > 3) return c;
  j = m % 10;
  if(j < 1 && i > 3) return c;
  for(k=0; k<j; k++) {
    switch(i) {
      case 1 :
        rotate4(&c.color[5][0], &c.color[5][1], &c.color[5][2], &c.color[5][3]);
        rotate8(&c.color[3][2], &c.color[3][1], &c.color[2][2], &c.color[2][1],
                &c.color[1][3], &c.color[1][2], &c.color[4][3], &c.color[4][2]);
        break;
      case 2 :
        rotate4(&c.color[4][0], &c.color[4][1], &c.color[4][2], &c.color[4][3]);
        rotate8(&c.color[0][3], &c.color[0][2], &c.color[3][3], &c.color[3][2],
                &c.color[5][3], &c.color[5][2], &c.color[1][2], &c.color[1][1]);
        break;
      case 3 :
        rotate4(&c.color[3][0], &c.color[3][1], &c.color[3][2], &c.color[3][3]);
        rotate8(&c.color[4][2], &c.color[4][1], &c.color[0][2], &c.color[0][1],
                &c.color[2][3], &c.color[2][2], &c.color[5][0], &c.color[5][3]);
        break;
      default :
        break;
    }
  }
  return c;
}
