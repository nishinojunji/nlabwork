/* cube_bfs.c : 2x2 Rubik's cube solver */
/* Rin Sagehashi */
/* 2016/5/11 */

typedef unsigned long long Cube;
typedef unsigned char Move;
struct node {
  struct node* next;
  struct node* Parent;
  unsigned data;
  unsigned long long path;
};

typedef struct {
  struct node* front;
  struct node* rear;
  int size;
} queue;
typedef unsigned int* bit;


/* ---- prototype ---- */

unsigned solve(void);
unsigned move(unsigned u, Move m);
queue init_openlist(void);
bit init_bit(int amount); //amount だけのbit量をunsigned intの配列で確保する関数
int get_bit(bit b, unsigned index); //index で指定されたbitが1なら1を、0なら0を返す関数
void set_bit(bit b, unsigned index); //index で指定されたbitを1にする関数
// void unset_bit(bit b, unsigned index); //index で指定されたbitを0にする関数 ※ただし使う予定はない
void enqueue(queue* q, unsigned x);
unsigned dequeue(queue* q);
Cube decompress(unsigned u);
void display(unsigned u);

/* ---- main ---- */

int main(void) {
  unsigned result = solve();
  display(result);
}


/* ---- function ---- */

#include <stdio.h>
#include <stdlib.h>

#define is_empty(a) a.size <= 0

unsigned solve(void) {
  queue openlist = init_openlist();
  bit visited = init_bit(11022480); //11022480=3^7*7!
  unsigned old, new;
  //Cube c;
  int count=0;
  int i;
  set_bit(visited, 0);
  while(1) {
    old = dequeue(&openlist);
    count++;
    for(i=0; i<6; i++) {
      new = move(old, i);
      //if((new&0xffffff) > 11022480) { printf("move(%u, %d) = %u\n", old, i, new); return;}
      if(get_bit(visited, new)) continue;
      set_bit(visited, new);
      enqueue(&openlist, new);
    }
    if(is_empty(openlist)) {
      printf("id: %u\n", old&0xffffff);
      printf("count: %d\n", count);
      printf("%u\n", old>>24&0xf);
      break;
    }
  }
  return old;
}
void display(unsigned u) {
  Cube c = decompress(u&0xffffff);
  int block[7] = {c&7, c>>3&7, c>>6&7, c>>9&7, c>>12&7, c>>15&7, c>18&7};
  int alignment[7] = {c>>21&3, c>>23&3, c>>25&3, c>>27&3, c>>29&3, c>>31&3, c>>33&3};
  int colors[7] = {0321,0431,0541,0251,0346,0456,0526};
  Cube color[3][7] = {0};
  int i;
  for(i=0; i<7; i++) {
    color[0][i] = colors[i]>>(3*(alignment[i]-1))&7;
    color[1][i] = colors[i]>>(3*((alignment[i]-1+1)%3))&7;
    color[2][i] = colors[i]>>(3*((alignment[i]-1+2)%3))&7;
  }
  c = 0;
  c = c | color[0][block[0]]<<18 | color[1][block[0]]<<54 | color[2][block[0]]<<42
        | color[0][block[1]]<<21 | color[1][block[1]]<<39 | color[2][block[1]]<<6
        | color[0][block[2]]<<12 | color[1][block[2]]<<3  | color[2][block[2]]<<30
        | color[0][block[3]]<<15 | color[1][block[3]]<<27 | color[2][block[3]]<<57
        | color[0][block[4]]<<51 | color[1][block[4]]<<9  | color[2][block[4]]<<36
        | color[0][block[5]]<<48 | color[1][block[5]]<<33 | color[2][block[5]]<<0
        | color[0][block[6]]<<45 | color[1][block[6]]<<60 | color[2][block[6]]<<24;
  
  //g=1,r=2,w=3,o=4,y=5,b=6
  char* col1[7], *col2[7], *col5[7], *logo[6];
  col1[0] = " \e[49m";
  col1[4] = "\e[48;5;202m \e[49m";//orange
  col1[5] = "\e[43m \e[49m";//yellow
  col1[1] = "\e[48;5;22m \e[49m";//green
  col1[3] = "\e[47m \e[49m";//white
  col1[6] = "\e[48;5;19m \e[49m";//blue
  col1[2] = "\e[41m \e[49m";//red 

  col2[0] = "  \e[49m";
  col2[4] = "\e[48;5;202m  \e[49m";
  col2[5] = "\e[43m  \e[49m";
  col2[1] = "\e[48;5;22m  \e[49m";
  col2[3] = "\e[47m  \e[49m";
  col2[6] = "\e[48;5;19m  \e[49m";
  col2[2] = "\e[41m  \e[49m";

  col5[0] = "     \e[49m";
  col5[4] = "\e[48;5;202m     \e[49m";
  col5[5] = "\e[43m     \e[49m";
  col5[1] = "\e[48;5;22m     \e[49m";
  col5[3] = "\e[47m     \e[49m";
  col5[6] = "\e[48;5;19m     \e[49m";
  col5[2] = "\e[41m     \e[49m";

  logo[0] = "\e[30;47mRUBIK\e[49;39m";
  logo[1] = "\e[30;47mS.COM\e[49;39m";
  logo[2] = "\e[48;5;19mRUBIK\e[49;39m";
  logo[3] = "\e[48;5;19mS.COM\e[49;39m";
  logo[4] = "\e[48;41mRUBIK\e[49;39m";
  logo[5] = "\e[48;41mS.COM\e[49;39m";

  printf("                   +-----+-----+++-----+-----+\n");
  printf("                  /%s/%s//||%s|%s|\n", col5[(c>>18)&07], col5[(c>>15)&07], col5[(c>>57)&07], col5[(c>>54)&07]);
  printf("                 /%s/%s//%s||%s|%s|\n", col5[(c>>18)&07], col5[(c>>15)&07], col1[(c>>27)&07], col5[(c>>57)&07], col5[(c>>54)&07]);
  printf("                +-----+-----++%s++-----+-----+\n", col2[(c>>27)&07]);
  printf("               /%s/%s//|%s/||%s|%s|\n",col5[(c>>21)&07],col5[(c>>12)&07],col1[(c>>27)&07],col5[(c>>60)&07],logo[4]);
  printf("              /%s/%s//%s|/%s||%s|%s|\n",col5[(c>>21)&07],col5[(c>>12)&07],col1[(c>>30)&07],col1[(c>>24)&07],col5[(c>>60)&07],logo[5]);
  printf("+-----+-----++=====+=====++%s+%s++-----+-----+\n", col2[(c>>30)&07], col2[(c>>24)&07]);
  printf("|%s|%s||%s|%s||%s/|%s/\n", col5[(c>>42)&07], col5[(c>>39)&07], col5[(c>>6)&07], col5[(c>>3)&07], col1[(c>>30)&07], col1[(c>>24)&07]);
  printf("|%s|%s||%s|%s||/%s|/\n", col5[(c>>42)&07], col5[(c>>39)&07], col5[(c>>6)&07], col5[(c>>3)&07], col1[(c>>33)&07]);
  printf("+-----+-----++-----+-----++%s+\n", col2[(c>>33)&07]);
  printf("|%s|%s||%s|%s||%s/\n", logo[0], col5[(c>>36)&07], col5[(c>>9)&07], col5[(c>>0)&07], col1[(c>>33)&07]);
  printf("|%s|%s||%s|%s||/\n", logo[1], col5[(c>>36)&07], col5[(c>>9)&07], col5[(c>>0)&07]);
  printf("+-----+-----++=====+=====++\n");
  printf("             |%s|%s|\n", col5[(c>>51)&07], col5[(c>>48)&07]);
  printf("             |%s|%s|\n", col5[(c>>51)&07], col5[(c>>48)&07]);
  printf("             +-----+-----+\n");
  printf("             |%s|%s|\n", logo[2], col5[(c>>45)&07]);
  printf("             |%s|%s|\n", logo[3], col5[(c>>45)&07]);
  printf("             +-----+-----+\n");


}
void enqueue(queue* q, unsigned x) {
  struct node* tmp;
  if((tmp = (struct node*)malloc(sizeof(struct node))) == NULL) {
    printf("enqueue error\n");
    exit(1);
  }
  tmp->data = x;
  //tmp->path = 0 | (x>>28&7)<<((x>>24&0xf)*3);
  tmp->next = NULL;
  if(q->rear == NULL) {
    q->front = tmp;
    q->rear = tmp;
  }
  q->rear->next = tmp;
  q->rear = tmp;
  q->size++;
}

unsigned dequeue(queue* q) {
  if(q->size <= 0) {
    printf("queue is empty\n");
    return 0xffffffff;
  }
  struct node* tmp = q->front;
  unsigned result = tmp->data;
  
  if(q->front == q->rear) {
    q->front = q->rear = NULL;
  } else {
    q->front = q->front->next;
  }
  free(tmp);
  q->size--;
  return result;
}

queue init_openlist(void) {
  queue newq;
  struct node* newnode = (struct node*)malloc(sizeof(struct node));
  
  newnode->data = 0;
  newnode->next = NULL; 
  newq.front = newnode;
  newq.rear = newnode;
  newq.size = 1;
  
  return newq;
}

void set_bit(bit b, unsigned index) {
  index = index & 0xffffff;
  b[index/32] |= (1<<index%32);
}

int get_bit(bit b, unsigned index) {
  index = index & 0xffffff;
  return (b[index/32]>>(index%32))&1;
}

bit init_bit(int amount) {
  int i;
  bit b;
  b = (bit)malloc(sizeof(unsigned)*amount/32+1);
  for(i=0; i<(amount/32+1); i++) b[i]=0;
  return b;
}

Cube rotate(Cube c, int b1, int b2, int b3, int b4) {
  return c & ~(07llu<<3*b1 | 07llu<<3*b2 | 07llu<<3*b3 | 07llu<<3*b4 | 3llu<<(2*b1+21) | 3llu<<(2*b2+21) | 3llu<<(2*b3+21) | 3llu<<(2*b4+21))
           | (c>>3*b1 & 07)<<3*b2 | (c>>3*b2 & 07)<<3*b3 | (c>>3*b3 & 07)<<3*b4 | (c>>3*b4 & 07)<<3*b1
           | (((c>>(2*b1+21)&3)==3)?1:((c>>(2*b1+21)&3)+1))<<(2*b2+21)
           | (((c>>(2*b2+21)&3)==1)?3:((c>>(2*b2+21)&3)-1))<<(2*b3+21)
           | (((c>>(2*b3+21)&3)==3)?1:((c>>(2*b3+21)&3)+1))<<(2*b4+21)
           | (((c>>(2*b4+21)&3)==1)?3:((c>>(2*b4+21)&3)-1))<<(2*b1+21);
}
unsigned compress(Cube c) {
  unsigned u = 0;
  int order[7] = {0}; // ブロックの順番を記録する
  int number[7] = {0, 1, 2, 3, 4, 5, 6};
  int i, j, tmp;
  for(i=0; i<7; i++) {
    tmp= (c>>i*3)&07;
    for(j=tmp; j<7; j++) {
      number[j] -= 1;
    }
    order[i] = number[tmp-1];
  }

  u = 720*order[0]+120*order[1]+24*order[2]+6*order[3]+2*order[4]+1*order[5];
  u = (((c>>21)&3)-1)*729+(((c>>23)&3)-1)*243+(((c>>25)&3)-1)*81+(((c>>27)&3)-1)*27
    + (((c>>29)&3)-1)*9+(((c>>31)&03)-1)*3+(((c>>33)&3)-1)*1
    + u * 2187;
  // gcc では 3=0b11 とも表現できる
  return u;
}

Cube decompress(unsigned u) {
  unsigned upper = u/2187; // 階乗の方
  unsigned long long downer = u%2187; // 累乗の方
  Cube c=0;
  int i, n, fact=720, pow=729, tmp=28;
  unsigned number = 07654321;
  
  for(i=0; i<6; i++) {
    n = upper/fact;
    c = c | (number>>(n*3)&7)<<(3*i) | (downer/pow+1)<<(21+2*i);
    tmp -= number>>(n*3)&7;
    number = number>>3 & (~0u)<<(n*3) | number & ~((~0u)<<(n*3));
    upper  %= fact;
    downer %= pow;
    fact /= 6-i;
    pow /= 3;
  }
  c = c | tmp << 18 | (downer+1) << 33;
 
  return c;
}

unsigned move(unsigned u, Move m) {
  unsigned count = u>>24 &0xf; 
  Cube c = decompress(u&0xffffff);
  switch(m) {
    case  0 : //上の面を反時計回り
      c = c & ~07777 | (c<<3 & 07770) | (c>>9 & 07);
      c = c & ~(0xff<<21) | (c<<2 & (077<<23)) | (c>>6 & (3<<21));
      break;
    case  1 : //前の面を反時計回り
      c = rotate(c,1,4,5,2);
      break;
    case  2 : //横の面を反時計回り
      c = rotate(c,2,5,6,3);
      break;
    case  3 : //上の面を時計回り
      c = c & ~07777 | (c>>3 & 0777) | (c<<9 & 07000);
      c = c & ~(0xff<<21) | (c>>2 & (077<<21)) | (c<<6 & (3<<27));
      break;
    case  4 : //前の面を時計回り
      c = rotate(c,5,4,1,2);
      break;
    case  5 : //横の面を時計回り
      c = rotate(c,6,5,2,3);
      break;
    default :
      break;
  }
  
  return compress(c) | (count+1)<<24 | m << 28;
}

