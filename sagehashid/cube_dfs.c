/* cube_solver.c : 2x2 Rubik's cube solver */
/* Rin Sagehashi */
/* 2016/4/22 */
typedef unsigned long long Cube;
typedef int Move;

Move path[60] = {0};
Cube visited[200000000] = {0};
int p_tail = 0;
int v_tail = 0;

/* ---- prototype ---- */

Cube cube_init(int argc, char* argv[]);
void cube_display(Cube c);
Move user();
Cube move(Cube c, Move m);
void cube_solve(Cube c);


/* ---- main ---- */

int main(int argc, char* argv[]) {
  Cube c;
  Move m;
  
  c = cube_init(argc, argv);
  //cube_display(c);
  cube_solve(c);
  
}


/* ---- function ---- */
#include <stdio.h>
#include <stdlib.h>
/*
void print_path(int depth) {
  int i;
  puts("----");
  for(i=0; i<depth; i++) {
    printf("%d\n", path[i]);
  }
}

int is_visited(Cube c, int depth) {
  int i;
  
  for(i=0; i<v_tail; i++) {
    if(c == visited[i]) {
      printf("%022llo\t%d\t%d\n", c, v_tail, depth);
      if(i==0) {
        print_path(depth);
        exit(1);
      }
      return 1;
    }
  }
  return 0;
}



void dfs(Cube c, Move m, int depth) { // cは現在状態
  int i;
  if(depth>20)return;
  c = move(c,m);
  path[depth] = m;
  //printf("%022llo\n", c);
  
  if(is_visited(c, depth)) return;
  else visited[v_tail++] = c;
  for(i=0; i<3; i++) {
    //path[p_tail] = i;
    //p_tail+=1;
    //printf("%022llo\t%d\t%d\n", c, v_tail, p_tail);
    dfs(c,i,depth+1);
    //path[p_tail] = -1;
    //p_tail-=1;
  }
}

void cube_solve(Cube c) {
  int i;
  printf("start\n");
  
  visited[0] = 0666555444222233331111; //正解
  visited[1] = c;
  v_tail = 2;

  for(i=0; i<3; i++) {
   //path[p_tail] = i;
   //p_tail+=1;
   dfs(c, i, 0);
   //path[p_tail] = -1;
   //p_tail-=1;	
  }
}
*/

void print_path() {
  int i;
  puts("----");
  for(i=0; i<p_tail; i++) {
    printf("%d\n", path[i]);
  }
}

int is_visited(Cube c) {
  int i;
  
  for(i=0; i<v_tail; i++) {
    if(c == visited[i]) {
      //printf("%022llo\t%d\t%d\n", c, v_tail, p_tail);
      if(i==0) {
        print_path();
        exit(1);
      }
      return 1;
    }
  }
  return 0;
}



void dfs(Cube c, Move m) { // cは現在状態
  int i;
  if(p_tail>30)return;
  c = move(c,m);
  //printf("%022llo\n", c);
  
  if(is_visited(c)) return;
  else visited[v_tail++] = c;
  for(i=0; i<3; i++) {
    path[p_tail] = i;
    p_tail+=1;
    //printf("%022llo\t%d\t%d\n", c, v_tail, p_tail);
    dfs(c,i);
    path[p_tail] = -1;
    p_tail-=1;
  }
}

void cube_solve(Cube c) {
  int i;
  printf("start\n");
  
  visited[0] = 0666555444222233331111; //正解
  visited[1] = c;
  v_tail = 2;

  for(i=0; i<3; i++) {
   path[p_tail] = i;
   p_tail+=1;
   dfs(c, i);
   path[p_tail] = -1;
   p_tail-=1;
  }

}

Cube cube_input() {
  Cube c =0, m;
  int i, j=0;
  char* rubik[] = {
"                   +-----+-----+++-----+-----+\n",
"                  /     /     //||     |     |\n",
"                 / 18  / 15  // || 57  | 54  |\n",
"                +-----1-----++27++-----5-----+\n",
"               /     /     //| /||     |rrrrr|\n",
"              / 21  / 12  // |/ || 60  |rrrrr|\n",
"+-----+-----++=====+=====++30+24++-----+-----+\n",
"|     |     ||     |     || /2 /\n",
"| 42  | 39  || 06  | 03  ||/ |/\n",
"+-----3-----++-----0-----++33+\n",
"|wwwww|     ||     |     || /\n",
"|wwwww| 36  || 09  | 00  ||/\n",
"+-----+-----++=====+=====++\n",
"             |     |     |\n",
"             | 51  | 48  |\n",
"             +-----3-----+\n",
"             |bbbbb|     |\n",
"             |bbbbb| 45  |\n",
"             +-----+-----+\n"
};
/* 0	 */
  do{
    for(i=0; i<19; i++) printf("%s", rubik[i]);
    puts("[1]orange [2]yellow [3]green");
    puts("[4]white  [5]blue   [6]red");
    puts("[100]現在の配置をチェック");
    printf("%02d: ", j*3);
    m = (Cube)user();
    if(m == 100) cube_display(c);
    else if(m > 0 && m < 7) {
      c = c | (m<<(j*3));
      j++;
    }
  } while(c < 0100000000000000000000);
  return c;
}

Cube cube_init(int argc, char* argv[]) {
  Cube c;
  FILE* fp;
  char buf[100];

  if(argc >= 2 && (fp = fopen(argv[1], "r")) != NULL) {
    fgets(buf, 100, fp);
    sscanf(buf, "%llo", &c);
    return c;
  }

  c = cube_input();

  return c;
}

void cube_display(Cube c) {
  int i;
  char* col1[7], *col2[7], *col5[7], *logo[6];
  col1[0] = " \e[49m";
  col1[1] = "\e[48;5;202m \e[49m";
  col1[2] = "\e[43m \e[49m";
  col1[3] = "\e[48;5;22m \e[49m";
  col1[4] = "\e[47m \e[49m";
  col1[5] = "\e[48;5;19m \e[49m";
  col1[6] = "\e[41m \e[49m";

  col2[0] = "  \e[49m";
  col2[1] = "\e[48;5;202m  \e[49m";
  col2[2] = "\e[43m  \e[49m";
  col2[3] = "\e[48;5;22m  \e[49m";
  col2[4] = "\e[47m  \e[49m";
  col2[5] = "\e[48;5;19m  \e[49m";
  col2[6] = "\e[41m  \e[49m";

  col5[0] = "     \e[49m";
  col5[1] = "\e[48;5;202m     \e[49m";
  col5[2] = "\e[43m     \e[49m";
  col5[3] = "\e[48;5;22m     \e[49m";
  col5[4] = "\e[47m     \e[49m";
  col5[5] = "\e[48;5;19m     \e[49m";
  col5[6] = "\e[41m     \e[49m";

  logo[0] = "\e[30;47mRUBIK\e[49;39m";
  logo[1] = "\e[30;47mS.COM\e[49;39m";
  logo[2] = "\e[48;5;19mRUBIK\e[49;39m";
  logo[3] = "\e[48;5;19mS.COM\e[49;39m";
  logo[4] = "\e[48;41mRUBIK\e[49;39m";
  logo[5] = "\e[48;41mS.COM\e[49;39m";

  system("clear");
  printf("                   +-----+-----+++-----+-----+\n");
  printf("                  /%s/%s//||%s|%s|\n", col5[(c>>18)&07], col5[(c>>15)&07], col5[(c>>57)&07], col5[(c>>54)&07]);
  printf("                 /%s/%s//%s||%s|%s|\n", col5[(c>>18)&07], col5[(c>>15)&07], col1[(c>>27)&07], col5[(c>>57)&07], col5[(c>>54)&07]);
  printf("                +-----+-----++%s++-----+-----+\n", col2[(c>>27)&07]);
  printf("               /%s/%s//|%s/||%s|%s|\n",col5[(c>>21)&07],col5[(c>>12)&07],col1[(c>>27)&07],col5[(c>>60)&07],logo[4]);
  printf("              /%s/%s//%s|/%s||%s|%s|\n",col5[(c>>21)&07],col5[(c>>12)&07],col1[(c>>30)&07],col1[(c>>24)&07],col5[(c>>60)&07],logo[5]);
  printf("+-----+-----++=====+=====++%s+%s++-----+-----+\n", col2[(c>>30)&07], col2[(c>>24)&07]);
  printf("|%s|%s||%s|%s||%s/|%s/\n", col5[(c>>42)&07], col5[(c>>39)&07], col5[(c>>6)&07], col5[(c>>3)&07], col1[(c>>30)&07], col1[(c>>24)&07]);
  printf("|%s|%s||%s|%s||/%s|/\n", col5[(c>>42)&07], col5[(c>>39)&07], col5[(c>>6)&07], col5[(c>>3)&07], col1[(c>>33)&07]);
  printf("+-----+-----++-----+-----++%s+\n", col2[(c>>33)&07]);
  printf("|%s|%s||%s|%s||%s/\n", logo[0], col5[(c>>36)&07], col5[(c>>9)&07], col5[(c>>0)&07], col1[(c>>33)&07]);
  printf("|%s|%s||%s|%s||/\n", logo[1], col5[(c>>36)&07], col5[(c>>9)&07], col5[(c>>0)&07]);
  printf("+-----+-----++=====+=====++\n");
  printf("             |%s|%s|\n", col5[(c>>51)&07], col5[(c>>48)&07]);
  printf("             |%s|%s|\n", col5[(c>>51)&07], col5[(c>>48)&07]);
  printf("             +-----+-----+\n");
  printf("             |%s|%s|\n", logo[2], col5[(c>>45)&07]);
  printf("             |%s|%s|\n", logo[3], col5[(c>>45)&07]);
  printf("             +-----+-----+\n");

}

Move user() {
  Move m;
  char buf[80];
  
  fgets(buf, 80, stdin);
  sscanf(buf, "%d", &m);

  return m;
}
Cube move(Cube c, Move m){
  /* 反時計回り */
  switch(m) {
    case 0:
      c = c & (~07777) | (((c<<3) & 07770) | ((c>>9) & 07));
      c = c & (~0770077770070070000) | ((c&070000)<<27) | ((c&070000000)<<15) | ((c&070000000000)>>9)
                                       | ((c&0700000000000)>>21) | ((c&077000000000000)<<12) | ((c&0770000000000000000)>>18);
      break;
    case 1:
      c = c & (~077770000) | (((c<<3) & 077700000) | ((c>>9) & 070000));
      c = c&(~077000770077000000770)|((c&0770)<<24)|((c&077000000000)<<27)|((c&0770000000000000)>>36)|((c&077000000000000000000)>>15);
      break;
    case 2:
      c = c & (~0777700000000) | (((c<<3) & 0777000000000) | ((c>>9) & 0700000000));
      c = c & (~0770077000000000770077)|((c&077)<<45)|((c&0770000)>>12)|((c&077000000000000000)<<12)|((c&0770000000000000000000)>>45);
      break;
    default:
      break;
  }
  return c;
}

/*
1:orange
2:yellow
3:green
4:white
5:blue
6:red
*/
/*

               +----+----+++----+----+
              /gggg/gggg//||rrrr|rrrr|
             +----+----++y++----+----+
            /gggg/gggg//|/||rrrr|rrrr|
+----+----++====+====++y+y++----+----+
|wwww|wwww||oooo|oooo||/|/
+----+----++----+----++y+
|wwww|wwww||oooo|oooo||/
+----+----++====+====++
           |bbbb|bbbb|
           +----+----+
           |bbbb|bbbb|
           +----+----+
*/
/*
                   +-----+-----+++-----+-----+
                  /     /     //||     |     |
                 / 18  / 15  // || 57  | 54  |
                +-----1-----++27++-----5-----+
               /     /     //| /||     |rrrrr|
              / 21  / 12  // |/ || 60  |rrrrr|
+-----+-----++=====+=====++30+24++-----+-----+
|     |     ||     |     || /2 /
| 42  | 39  || 06  | 03  ||/ |/
+-----3-----++-----0-----++33+
|wwwww|     ||     |     || /
|wwwww| 36  || 09  | 00  ||/
+-----+-----++=====+=====++
             |     |     |
             | 51  | 48  |
             +-----3-----+
             |bbbbb|     |
             |bbbbb| 45  |
             +-----+-----+
*/

