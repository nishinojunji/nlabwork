/* rensyu1_sagehashi.c : sum primes in [a:b] */
/* Rin Sagehashi */
/* 2016/4/13 */
/* 信頼できる入力しかされないと仮定 */

typedef struct {long a; long b;} Inputs;
typedef long long Result;

/* ---- prototypes ---- */
Inputs input();
Result sum_primes(Inputs in);
void output(Result result);


/* ---- main ---- */
int main() {
  Inputs in = input();
  Result result = sum_primes(in);
  output(result);
}


/* ---- function ---- */
#include<stdio.h>
#include<math.h>

Inputs input() {
  Inputs in;
  /* 信用できる入力しかされないものとする */
  scanf("%ld", &in.a);
  scanf("%ld", &in.b);
	
  return in;
}

long is_prime(long n, int* primes) {
  int i;
  for(i=0; primes[i] > 0; i++) {
    if(n % primes[i] == 0) {
      if(primes[i] == n) return n;
      return 0;
    }
  }
  return n;
}

void make_primes(int max, int* primes) {
  int i=7;
  int tail = 3;

  primes[0]=2; primes[1]=3; primes[2]=5;
  while(i<=max) {
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 4;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 2;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 4;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 2;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 4;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 6;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 2;
    if((primes[tail] = is_prime(i, primes)) != 0) tail++;
    i += 6;
  }

}

Result sum_primes(Inputs in) {
  int primes[2000000] = {0};
  Result sum = 0;
  int tail = 3;
  int i;
  long li;

  if(in.a > in.b) return 0;
  if(in.b < 2) return 0;
  if(in.a < 2) in.a = 2;

  make_primes(sqrt(in.b), primes);

  for(li=in.a; li<=in.b; li++) {
    sum += is_prime(li, primes);
  }

  return sum;
}

void output(Result result) {
  printf("%lld\n", result);
}


