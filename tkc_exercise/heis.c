#include <stdio.h>
#include <time.h>
#include <stdlib.h>

typedef struct {
  int enemy;
  int HP;
  int east;
  int west;
  int south;
  int north;
  int move;
  int attack;
} soldier;

typedef struct {
  int count;
  int is_attack;
} AI_mem;

typedef struct {
  soldier pos[20][20];
  AI_mem mem;
} board;

typedef struct {
  int tempX;
  int tempY;
} input;

int abs(int x) {
  if(x < 0) return (-1) * x;
  else return x;
}

board initgame(board b) {
  int x, y;
  for(x = 0; x < 6; x++)
    for(y = 0; y < 6; y++) {
      b.pos[x][y].enemy = -1;
      b.pos[x][y].HP = 2;
    }
  for(x = 14; x < 20; x++)
    for(y = 14; y < 20; y++) {
      b.pos[x][y].enemy = 1;
      b.pos[x][y].HP = 2;
    }
  for(x = 0; x < 20; x++)
    for(y = 0; y < 20; y++) {
      if(x < 19) b.pos[x][y].east = b.pos[x + 1][y].enemy;
      if(x > 0) b.pos[x][y].west = b.pos[x - 1][y].enemy;
      if(y < 19) b.pos[x][y].south = b.pos[x][y + 1].enemy;
      if(y > 0) b.pos[x][y].north = b.pos[x][y - 1].enemy;
      //      printf("%d %d %d %d\n", b.pos[x][y].east, b.pos[x][y].west, b.pos[x][y].south, b.pos[x][y].north);
    }
  return b;
}

board initaction(board b, int e) {
  int x, y;
  for(x = 0; x < 20; x++)
    for(y = 0; y < 20; y++) 
      if(b.pos[x][y].enemy == e) {
	b.pos[x][y].move = 0;
	b.pos[x][y].attack = 0;
      } 
  return b;
}

void printboard(board b){
  int x, y;

  printf("  |");
  for(x = 0; x < 20; x++) {
    printf("%d", x + 1);
    if(x < 9) printf(" ");
    printf("|");
  }
  printf("\n");
	   
  for(y = 0; y < 20; y++) {
    printf("%d", y + 1);
    if(y < 9) printf(" ");
    for(x = 0; x < 20; x++) {
      printf("|");
      if(b.pos[x][y].enemy == -1) {
	if(b.pos[x][y].attack) 
	  if(b.pos[x][y].move) printf("_"); 
	  else printf("O");
	else if(b.pos[x][y].move) printf("Q");
	else printf("@");
      }
      if(b.pos[x][y].enemy == 0) printf(" ");
      if(b.pos[x][y].enemy == 1) {
	if(b.pos[x][y].attack)
	  if(b.pos[x][y].move) printf("'");
	  else printf("\"");
	else if(b.pos[x][y].move) printf("^");
	else printf("*");
      }
      if(b.pos[x][y].HP == 0) printf("+");
      else printf("%d", b.pos[x][y].HP);
    }
    printf("|\n");
  }
}

soldier empty() {
  soldier s = {};
  return s;
}

int obstacle(board b, int x1, int x2, int y1, int y2, int message) {
  int px, py;
  px = x1;  py = y1;  //プリント用
  x1--; x2--; y1--; y2--;
  if(y1 - y2 == 0) {
    if(x2 > x1) 
      if(b.pos[x1][y1].east + b.pos[x1][y1].enemy == 0) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px + 1, py);
	return 1;
      }
      else if((x2 - x1 > 2) && (b.pos[x1 + 1][y1].east + b.pos[x1][y1].enemy == 0)) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px + 2, py);
	return 1;
      }
    if(x2 < x1) 
      if(b.pos[x1][y1].west + b.pos[x1][y1].enemy == 0) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px - 1, py);	
	return 1;
      }
      else if((x1 - x2 > 2) && (b.pos[x1 - 1][y1].west + b.pos[x1][y1].enemy == 0)) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px - 2, py);
	return 1;
      }
  }
  if(x1 - x2 == 0) {
    if(y2 > y1) 
      if(b.pos[x1][y1].south + b.pos[x1][y1].enemy == 0) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px, py + 1);
	return 1;
      }
      else if((y2 - y1 > 2) && (b.pos[x1][y1 + 1].south + b.pos[x1][y1].enemy == 0)) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px, py + 2);
	return 1;
      }
    if(y2 < y1) 
      if(b.pos[x1][y1].north + b.pos[x1][y1].enemy == 0) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px, py - 1);
	return 1;
      }
      else if((y1 - y2 > 2) && (b.pos[x1][y1 - 1].north + b.pos[x1][y1].enemy == 0)) {
	if(message) printf("敵(%d, %d)によって阻まれます。\n", px, py - 2);
	return 1;
      }
  }
  return 0;
}

board action(board b, int e) {
  int x, y;
  int startX, startY, destX, destY, damageX, damageY;
  char yes, cont, dummy;

  while(1) {
    printf("行動する駒の座標を指定してください:");
    scanf("%d %d", &startX, &startY);
    scanf("%c", &dummy);
    if(b.pos[startX - 1][startY - 1].enemy != e || (b.pos[startX - 1][startY - 1].move + b.pos[startX - 1][startY - 1].attack) != 0 || startX > 20 || startY > 20) {
      printf("無効な入力です\n");
      printf("入力を続けますか(Y?):");
      scanf("%c", &cont);
      scanf("%c", &dummy);
      if(cont != 'Y') return b;
    }
    else break;
  }
  printf("駒を動かしますか(Y?):");
  scanf("%c", &yes);
  scanf("%c", &dummy);
  if(yes == 'Y') {
    while(1) {
      while(1) {
	printf("目的地の座標を指定してください:");
	scanf("%d %d", &destX, &destY);
	scanf("%c", &dummy);
	if(destX > 20 || destY > 20 || (abs(destX - startX) + abs(destY - startY)) > 3 || obstacle(b, startX, destX, startY, destY, 1)) {
	  printf("無効な入力です\n");
	  printf("入力を続けますか(Y?):");
	  scanf("%c", &cont);
	  scanf("%c", &dummy);
	  if(cont != 'Y') return b;
	} else if(b.pos[destX - 1][destY - 1].enemy != 0) {
	  printf("既に駒が存在します\n");
	  printf("入力を続けますか(Y?):");
	  scanf("%c", &cont);
	  scanf("%c", &dummy);
	  if(cont != 'Y') return b;
	} else break;
      }
      break;
    }
    b.pos[destX - 1][destY - 1] = b.pos[startX - 1][startY - 1];
    b.pos[destX - 1][destY - 1].move++;
    b.pos[startX - 1][startY - 1] = empty();
    
    for(x = startX - 4; x < startX + 2; x++) {
      if(x < 0) continue;
      if(x > 19) continue;
      for(y = startY - 4; y < startY + 2; y++) {
	if(y < 0) continue;
	if(y > 19) continue;
	if(x < 19) b.pos[x][y].east = b.pos[x + 1][y].enemy;
	if(x > 0) b.pos[x][y].west = b.pos[x - 1][y].enemy;
	if(y < 19) b.pos[x][y].south = b.pos[x][y + 1].enemy;
	if(y > 0) b.pos[x][y].north = b.pos[x][y - 1].enemy;
      }
    }
    printboard(b);
  } else {
    destX = startX;
    destY = startY;
  }
  printf("攻撃しますか(Y?):");
  scanf("%c", &yes);
  scanf("%c", &dummy);
  if(yes == 'Y') {
    while(1) {
      printf("対象の座標を指定してください:");
      scanf("%d %d", &damageX, &damageY);
      scanf("%c", &dummy);
      if((b.pos[destX - 1][destY - 1].enemy + b.pos[damageX - 1][damageY - 1].enemy) != 0 || (abs(destX - damageX) + abs(destY - damageY)) != 1) {
	printf("無効な入力です\n");
	printf("入力を続けますか(Y?):");
	scanf("%c", &cont);
	scanf("%c", &dummy);
	if(cont != 'Y') return b;
      } else break;
    } 
    
    b.pos[damageX - 1][damageY - 1].HP--;
    if(b.pos[damageX - 1][damageY - 1].HP == 0) {
      b.pos[damageX - 1][damageY - 1] = empty();
      for(x = damageX - 2; x <= damageX + 2; x++)
	for(y = damageY - 2; y <= damageY + 2; y++) {
	  b.pos[x - 1][y - 1].east = b.pos[x][y - 1].enemy;
	  b.pos[x - 1][y - 1].west = b.pos[x - 2][y - 1].enemy;
	  b.pos[x - 1][y - 1].south = b.pos[x - 1][y].enemy;
	  b.pos[x - 1][y - 1].north = b.pos[x - 1][y - 2].enemy;
	}
    }
    b.pos[destX - 1][destY - 1].attack++;
  }
  return b;
}

int judge(board b) {
  int x, y;
  int ewin = 0;
  for(x = 0; x < 20; x++) {
    if(ewin) break;
    for(y = 0; y < 20; y++)
      if(b.pos[x][y].enemy == -1) {
	ewin = 1;
	break;
      }
  }
  for(x = 0; x < 20; x++)
    for(y = 0; y < 20; y++)
      if(b.pos[x][y].enemy == 1) {
	if(ewin == 0) return 1;
	else return 0;
      }
  return 1;
}

board AI_move(int tempX, int tempY, int x, int y, board b){
  int tx, ty;
  if(b.pos[x][y].move || b.pos[x][y].attack) return b;
  b.pos[tempX][tempY] = b.pos[x][y];
  b.pos[x][y] = empty();
  b.pos[tempX][tempY].move++;
  for(tx = tempX - 3; tx <= tempX + 3; tx++)
    for(ty = tempY - 3; ty <= tempY + 3; ty++) {
      if(tx > 19 || ty > 19 || tx < 0 || ty < 0) continue;
      b.pos[tx][ty].east = b.pos[tx + 1][ty].enemy;
      b.pos[tx][ty].west = b.pos[tx - 1][ty].enemy;
      b.pos[tx][ty].south = b.pos[tx][ty + 1].enemy;
      b.pos[tx][ty].north = b.pos[tx][ty - 1].enemy;
      b.pos[19][ty].east = 0;
      b.pos[0][ty].west = 0;
      b.pos[tx][19].south = 0;
      b.pos[tx][0].north = 0;

    }
  b.mem.count++;
  return b;
}

board AI_attack(int damageX, int damageY, int x, int y, board b){
  int tx, ty;
  if(b.pos[x][y].attack) return b;
  b.pos[damageX][damageY].HP--;
  if(b.pos[damageX][damageY].HP == 0) {
    b.pos[damageX][damageY] = empty();
    for(tx = damageX - 2; tx <= damageX + 2; tx++)
      for(ty = damageY - 2; ty <= damageY + 2; ty++) {
	if(tx > 19 || ty > 19 || tx < 0 || ty < 0) continue;
	b.pos[tx][ty].east = b.pos[tx + 1][ty].enemy;
	b.pos[tx][ty].west = b.pos[tx - 1][ty].enemy;
	b.pos[tx][ty].south = b.pos[tx][ty + 1].enemy;
	b.pos[tx][ty].north = b.pos[tx][ty - 1].enemy;
	b.pos[19][ty].east = 0;
	b.pos[0][ty].west = 0;
	b.pos[tx][19].south = 0;
	b.pos[tx][0].north = 0;
      }
  }
  b.pos[x][y].attack++;
  if(b.pos[x][y].move == 0) b.mem.count++;
  b.mem.is_attack++;
  return b;
}

board AI(board b, int e) {
  int x, y, tempc = 1;
  int i, j, done;

  b.mem.count = 0;
  while(b.mem.count < 36 && (tempc != b.mem.count) || b.mem.is_attack != 0) {
    tempc = b.mem.count;
    b.mem.is_attack = 0;
    for(x = 0; x < 20; x++)
      for(y = 0; y < 20; y++)
	if(b.pos[x][y].enemy == e) {
	  //	  printf("(%d, %d) west:%d north:%d\n",x+1, y+1, b.pos[x][y].west, b.pos[x][y].north);
	  //	  printboard(b);
	  if(x - 1 < 0 || y - 1 < 0) {
	    if(b.pos[x][y].west == ((-1) * e)) b = AI_attack(x - 1, y, x, y, b);
	    else if(b.pos[x][y].north == ((-1) * e)) b = AI_attack(x, y - 1, x, y, b);
	    else if(b.pos[x][y].east == ((-1) * e)) b = AI_attack(x + 1, y, x, y, b);
	    else if(b.pos[x][y].south == ((-1) * e)) b = AI_attack(x, y + 1, x, y, b);
	    continue;
	  }
	  if(b.pos[x][y].west == ((-1) * e)) b = AI_attack(x - 1, y, x, y, b);
	  else if(b.pos[x][y].north == ((-1) * e)) b = AI_attack(x, y - 1, x, y, b);
	  else if(b.pos[x][y].east == ((-1) * e)) b = AI_attack(x + 1, y, x, y, b);
	  else if(b.pos[x][y].south == ((-1) * e)) b = AI_attack(x, y + 1, x, y, b);
	  
	  if(b.pos[x - 1][y - 1].enemy == (-1) * e) continue;
	  /*	  if(b.pos[x - 1][y - 1].enemy == 0) {
	    b = AI_move(x - 1, y - 1, x, y, b);
	    printf("count:%d\n", b.mem.count);
	    continue;
	    }*/
	  if(b.pos[x][y].move == 0 && ((x > 0 && y < 19 && (b.pos[x - 1][y + 1].enemy == 0)) || (x < 19 && y > 0 && (b.pos[x + 1][y - 1].enemy == 0)))) {
	    printf("%d done(%d, %d)\n", b.pos[x][y].move, x + 1, y + 1);
	    done = 0;
	    srand((unsigned)time(NULL));
	    if(rand() % 2 == 0) {
	      for(i = x + 3; i >= x - 3; i--) {
		if(done) break;
		for(j = y + 3; j >= y - 3; j--)
		  if((abs(i - x) + abs(j - y) <= 3) && i >= 0 && j >= 0 && i < 20 && j < 20 && b.pos[i][j].enemy == 0 && ((i > 0 && j > 0 && (b.pos[i - 1][j - 1].enemy == e)) || (i < 19 && j > 0 && (b.pos[i + 1][j - 1].enemy == e)) || (i < 19 && j < 19 && (b.pos[i + 1][j + 1].enemy)) || (i > 0 && j < 19 && (b.pos[i - 1][j + 1].enemy == e))) && obstacle(b, x+1, i+1, y+1, j+1, 0) == 0) {
		    printf("x:%d y:%d\n", i+1, j+1);
		    b = AI_move(i, j, x, y, b);
		    done = 1;
		    break;
		  }
	      }
	    }else {
	      for(i = x - 3; i <= x + 3; i++) {
		if(done) break;
		for(j = y + 3; j >= y - 3; j--)
		  if((abs(i - x) + abs(j - y) <= 3) && i >= 0 && j >= 0 && i < 20 && j < 20 && b.pos[i][j].enemy == 0 && ((i > 0 && j > 0 && (b.pos[i - 1][j - 1].enemy == e)) || (i < 19 && j > 0 && (b.pos[i + 1][j - 1].enemy == e)) || (i > 0 && j < 19 && (b.pos[i - 1][j + 1].enemy == e)) || (i < 19 && j < 19 && (b.pos[i + 1][j + 1].east))) && obstacle(b, x+1, i+1, y+1, j+1, 0) == 0) {
		    printf("x:%d y:%d\n", i+1, j+1);
		    b = AI_move(i, j, x, y, b);
		    done = 1;
		    break;
		  }
	      }
	    }
	  }
	  //	  printf("count:%d\n", b.mem.count);
	}
    //    printf("attack: %d\n", b.mem.is_attack);
    for(x = 0; x < 20; x++)
      for(y = 0; y < 20; y++) {
	if(x < 19) b.pos[x][y].east = b.pos[x + 1][y].enemy;
	if(x > 0) b.pos[x][y].west = b.pos[x - 1][y].enemy;
	if(y < 19) b.pos[x][y].south = b.pos[x][y + 1].enemy;
	if(y > 0) b.pos[x][y].north = b.pos[x][y - 1].enemy;
      }
  }
  int num = 0;
  for(x = 0; x < 20; x++)
    for(y = 0; y < 20; y++)
      if(b.pos[x][y].enemy == e) num++;
  printf("number:%d\n", num);
  return b;
}

int main(void) {
  board game = {};
  char yes, dummy;
  game = initgame(game);
  while(1) {
    printf("~~~player's turn~~~\n");
    game = initaction(game, -1);
    while(1) {
      printboard(game);
      printf("行動しますか(Y?):");
      scanf("%c", &yes);
      scanf("%c", &dummy);
      if(yes == 'Y') {
	game = action(game, -1);
	printboard(game);
	if(judge(game)) {
	  printboard(game);
	  printf("player won\n");
	  return 0;
	}
      }
      printf("続けて行動しますか(Y?):");
      scanf("%c", &yes);
      scanf("%c", &dummy);
      if(yes != 'Y') break;
    }
    
    printf("~~~enemy's turn~~~\n");
    game = initaction(game, 1);
    /*    while(1) {
	  printboard(game);
	  game = action(game, 1);
	  printboard(game);
	  if(judge(game)) {
	  printf("enemy won\n");
	  return 0;
	  }
	  printf("行動を終えますか(Y?):");
	  scanf("%c", &yes);
	  scanf("%c", &dummy);
	  if(yes == 'Y') break;
	  }    */
    game = AI(game, 1);
    if(judge(game)) {
      printboard(game);
      printf("enemy won\n");
      return 0;
    }
  }  
  return 0;
}
