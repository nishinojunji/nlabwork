#include <stdio.h>
#define NUMSIZE 1024 * 1024

// 構造体を使おう
typedef struct {unsigned int begin; unsigned int end;} Inputs;
// 関数を使おう
// prototypes
Inputs input();
unsigned int sum_of_prime(unsigned int begin, unsigned int end);
void output(unsigned int result);

Inputs input() {
  Inputs in;
  printf("beginning number:");
  scanf("%d", &in.begin);
  printf("end number:");
  scanf("%d", &in.end);
  return in;
}

unsigned int sum_of_prime(unsigned int begin, unsigned int end) {
  unsigned int i, j = 0, k, sum = 0;
  unsigned int num[NUMSIZE];

  num[0] = 3;
  if(begin < 3) sum = 5;
  if(begin == 3) sum = 3;

  for(i = 3; i <= end; i += 2) {
    for(k = 0; k <= j; k++) {
      if(i % num[k] == 0) break;
      if(num[k] * num[k] > i) {
	if(num[j] * num[j] <= end) {
	  num[j + 1] = i;
	  j++;
	}
	if(i >= begin) sum += i;
	break;
      }
    }
  }
  return sum;
}

void output(unsigned int result) {
  printf("result = %d\n", result);
}  

int main(void) {
  Inputs in;
  unsigned int result;

  in = input();
  result = sum_of_prime(in.begin, in.end);
  output(result);
  return 0;
}
