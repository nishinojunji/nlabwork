////// Kozu Takahiro's code
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define WriteList 0

typedef struct{
  unsigned long long b;
  unsigned long long e;
} Inputs;

int judge_Sosu(unsigned long long n);
Inputs input();
void judgeError(Inputs a);
unsigned long long summer(Inputs a);
void output(unsigned long long ans);
void writingList(Inputs a);
void findByArray(Inputs a);

int main(void)
{
  Inputs a;
  unsigned long long ans;
  a = input();
  system("date");
  if (WriteList==0) {
    ans = summer(a);
    output(ans);
  } else if (WriteList==1) {
    writingList(a);
  } else {
    printf("test before\n");
    findByArray(a);
  }
  system("date");
  return 0;  
}

int judge_Sosu(unsigned long long n)
{
  unsigned long long m;

  if (n==1) return 0;
  else if (n==2) return 1;
  else if (n%2==0) return 0;
  
  for (m=3;m<sqrt(n)+1;m+=2) {
    if (n%m==0) return 0;
  }
  
  return 1;
}

Inputs input()
{
  Inputs p;
  printf("[b, e] = ?\n");
  printf("|  k| m| g| t| p|   \n");
  scanf("%llu",&p.b);
  scanf("%llu",&p.e);
  judgeError(p);
  printf("[%llu , %llu]\n",p.b,p.e);
  return p;
}

void judgeError(Inputs a)
{
  if (a.b<1) {
    printf("please type over 0 for b\n");
    exit(1);
  }
  if (a.b>a.e) {
    printf("please type over %llu for e\n",a.b);
    exit(1);
  }
}

unsigned long long summer(Inputs a)
{
  unsigned long long n=a.b,sum=0;
  unsigned long long count;

  if (n==1) n++;
  if (n==2) {
    sum += n;
    n++;
  }
  if (n%2==0) n++;
  printf("begin\n");
  for (count=0;n<a.e+1;n+=2,count++) {
    if (judge_Sosu(n)) sum += n;
  }
  return sum;
}

void output(unsigned long long ans)
{
  printf("= finished =\n");
  printf("ans = %llu\n",ans);
  return;
}

void writingList(Inputs a)
{
  unsigned long long m=a.b;
  char z[64];
  FILE *fp;
  fp = fopen("K_primeList.txt","a");
  if (fp==NULL) exit(1);

  if (m==1) m++;
  if (m==2) {
    fputs("2\n",fp);
    m++;
  }
  if (m%2==0) m++;
  
  for (;m<a.e;m+=2) {
    if (judge_Sosu(m)) {
      sprintf(z,"%llu\n",m);
      fputs(z,fp);
    }
  }
  
  fclose(fp);  
  return;
}

void findByArray(Inputs a)
{
  printf("test after\n");
  long k[20000];
  long i=0;
  char str[64];
  FILE *fp;

   printf("test1\n");
  fp = fopen("K_primeList.txt","r");
  if (fp==NULL) exit(1);
  
  while (fgets(str,sizeof(str),fp) != NULL) {
    printf("test2\n");
    k[i] = atoi(str);
    i++;
  }
  
  printf("i = %ld\n",i);
  
  
  fclose(fp);
  return;
}
