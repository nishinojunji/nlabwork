/*rensyu1.c*/
/*amari yuta*/
/*[a:b]の素数の和を計算*/
/*記録 480000000-480001000か485000000-485001000*/
#include<stdio.h>

/******prototypes******/
typedef struct {unsigned int a; unsigned int b;} Inputs;

int prime_search(unsigned int a, unsigned int b, unsigned int num[]);
Inputs inputs();
unsigned long int cal_sum(unsigned int a, unsigned int b, unsigned int num[]);
unsigned int output(unsigned int a, unsigned int b, unsigned long int sum);

/******main******/
int main()
{ 
  unsigned long int i, sum;
  Inputs in;

  //入力
  in=inputs();

  unsigned int num[in.b-in.a+1];
  for(i=0;i<=in.b-in.a;i++) num[i]=in.a+i;

  //処理
  i = primes_search(in.a, in.b, num);

  sum = cal_sum(in.a, in.b, num);

  //出力
  i = output(in.a, in.b, sum);

  return 0;
}

/******function******/
int primes_search(unsigned int a, unsigned int b, unsigned int num[])
{
  unsigned int i, j;

  for(i=0;i<=b-a;i++)
    for(j=2;j<=b/2;j++)
      if((num[i]!=j && num[i]%j==0) || num[i]==1){
        num[i] = 0;
        break;
      }

  return 0;
}

Inputs inputs()
{
  Inputs in;

  printf("a = ");
  scanf("%u", &in.a);
  printf("b = ");
  scanf("%u", &in.b);

  return in;
}

unsigned long int cal_sum(unsigned int a, unsigned int b, unsigned int num[])
{
  unsigned long int i, sum=0;

  for(i=0;i<=b-a;i++) sum += num[i];

  return sum;
}

unsigned int output(unsigned int a, unsigned int b, unsigned long int sum)
{
  printf("[%u:%u]の素数の和:%lu\n", a, b, sum);

  return 0;
}
