/*rensyu1.c*/
/*amari yuta*/
/*[a:b]の素数の和を計算*/
#include<stdio.h>

/******prototypes******/
typedef struct {int a; int b;} Inputs;

int prime_search(int b, int num[]);
Inputs inputs();
int cal_sum(int a, int b, int num[]);
int output(int a, int b, int sum);

/******main******/
int main()
{
  int i, sum;
  Inputs in;

  //入力
  in=inputs();

  int num[in.b+1];
  for(i=0;i<=in.b;i++) num[i]=i;

  //処理
  i = primes_search(in.b, num);

  sum = cal_sum(in.a, in.b, num);

  //出力
  i = output(in.a, in.b, sum);

  return 0;
}

/******function******/
int primes_search(int b, int num[])
{
  int i, p;

  num[1] = 0;
  i = 2;
  while(i<=b/2){
    num[i*2] = 0;
    i++;
  }
  p = 3;
  while(p*p<=b){
    for(i=p;i<=b/p;i+=2) num[i*p] = 0; // このへんはわりと良い
    p++;
    while(num[p]==0) p++;
  }

  return 0;
}

Inputs inputs()
{
  Inputs in;

  printf("a = ");
  scanf("%d", &in.a);
  printf("b = ");
  scanf("%d", &in.b);

  return in;
}

int cal_sum(int a, int b, int num[])
{
  int i, sum=0;

  for(i=a;i<=b;i++) sum += num[i];

  return sum;
}

int output(int a, int b, int sum)
{
  printf("[%d:%d]の素数の和:%d\n", a, b, sum);

  return 0;
}
