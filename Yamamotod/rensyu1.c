
/* タイトル：指定された範囲内の素数の和を求めるプログラム */

//構造体や関数の使い方は、まだわからないので後日勉強してから修正します。
//確認済みの１分以内に処理できる最大の素数の和は約８０京。
//計算処理が正誤性について、他の人のプログラムを使って１００億以内で５種類確認。
//これから、２・３・５以外の素数が6n±1に入るのを利用して高速化予定。
#include <stdio.h>
#include <math.h>

int main(void)
{
  int yakusu,i,r;
  unsigned long long int sum,min,max;
  
  printf("範囲内の素数の和を求める。\n 範囲を入力してください。[a:b]\n");  
  printf("a=");
  scanf("%llu",&min);
  printf("b=");
  scanf("%llu",&max);
 if(max<=1 || min<=0){printf("範囲外、やり直し。"); return 0;}
  printf("範囲は%llu~%llu \n",min,max);
  // printf("範囲中の素数は");  
  if(min<=2 && 2<=max){
    // printf("2 ");
    sum+=2;
    min=3;
  }
   if(min%2==0){min=min+1;}
  
 for(min;min<=max;min+=2)
    {
		yakusu=0;
		r=sqrt(min);
		for(i=3;i<=r;i+=2)
		{
		   if(i%3==0 && i!=3){i+=2;}
		  // if(i%3==0 && i!=3){break;}
		   // if(i%5==0 && i!=5){i+=2;}
		  //if(i%7==0 && i!=7){break;}
		  
		  
			if(min%i==0)
			{
				yakusu+=1;
				break;
			}
		}

		if(yakusu==0) {
		  // printf("%llu \n ",min);
		  sum+=min;}
	}

 	printf("和は%lluです。\n",sum);

	return 0;
}
