/* rensyu1.c : sum primes in [a:b] */
/* Junji Nishino (nishinojunji@uec.ac.jp) */
/* 2016 */

typedef struct {
  long a;
  long b;
} Inputs;

/* ----- prototypes ----------- */
Inputs input();
long sum_primes(long a, long b);
void output(long result);


/* ----- main ----- */
int main(){
  Inputs in;
  long result;
  
  in = input();
  result = sum_primes(in.a, in.b);
  output(result);
}


/* ----- functions ----- */
#include<stdio.h>
Inputs input(){
  Inputs in;
  char buf[80];
  
  fgets(buf, 80, stdin);
  sscanf(buf, "%ld %ld", &in.a, &in.b);

  return in;
}


int is_prime(long a){
  long li;
  if(a <=1) return 0;
  for(li=2; li<a; li++){
    if(a % li == 0) return 0;
  }
  return 1;
}

long sum_primes(long a, long b){
  if(a>b) return 0;
  if(is_prime(a)){
    return a + sum_primes(a+1, b);
  }else{
    return sum_primes(a+1, b);
  }
}

void output(long n){
  char buf[80];

  sprintf(buf, "%ld\n", n);

  fputs(buf, stdout);
  
}
