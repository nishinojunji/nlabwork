/* タイトル：指定された範囲内の素数の和を求めるプログラム */

//構造体や関数の使い方は、まだわからないので後日勉強してから修正します。
//確認済みの１分以内に処理できる最大の素数の和は約８０京。
//計算処理が正誤性について、他の人のプログラムを使って１００億以内で５種類確認。
//これから、２・３・５以外の素数が6n±1に入るのを利用して高速化予定。
#include <stdio.h>
#include <math.h>

int main(void)
{
  int r,k=2;
  int yakusu,i,j;
  unsigned long long int sum,min,max;
  printf("範囲内の素数の和を求める。\n 範囲を入力してください。[a:b]\n");  
  printf("a=");
  scanf("%llu",&min);
  printf("b=");
  scanf("%llu",&max);
  printf("範囲は%llu~%llu \n",min,max);
  // printf("範囲中の素数は");  
  if(min<=2 && 2<=max){
    printf("2 ");
    sum+=2;
    min=3;
  }
  //if(min%2==0){//min=min+1;
    if(min%6==0){k=6;min=min+1;
    for(j=1;j>=5;j+4){
	for(min;min<=max;min+=k)
	  {
	    yakusu=0;
	    r=sqrt(min);
	    for(i=2;i<=r;i+=1)
	      {
		if(min%i==0)
		  {
		    yakusu+=1;
		    // break;
		  }
	      }
	    
	    if(yakusu==0) {
	      // printf("%llu ",min);
	      sum+=min;}
	  }
    }
  }
    printf("和は%lluです。\n",sum);
    
    return 0;
  }
