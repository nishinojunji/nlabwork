//AからBまでの数の間の素数の和を求める。
#include <stdio.h>
#define MAX 1001
//--- structures ---//
typedef struct{
  int s;
  int e;
} Inputs;
//---- prototype ---//
Inputs input(void);
int prime_search(Inputs a);
//----- main -----//
int main(void){

  Inputs a;
  int total;

  a = input();
  
  total = prime_search(a);

  printf("%dから%dまでの数の間の素数の和は%dです",a.s,a.e,total);
  return 0;
}

//--- functions ---//
int prime_search(Inputs a){
  int total=0;
  int table[MAX] = {1,1};
  int i,n=2;
  
  while(n<a.e){
    if(table[n]==0){
      if((a.s<=n)&&(n<=a.e)){
      total=total+n;
      }
      for(i=n;i<a.e;i=i+n) table[i]=1;
    }
    n++;
  }
  return total;
}

Inputs input(void){

  Inputs a;
  printf("下限の値を入力してください\n");
  scanf("%d",&a.s);
  printf("上限の値を入力してください\n");
  scanf("%d",&a.e);

  return a;
}
